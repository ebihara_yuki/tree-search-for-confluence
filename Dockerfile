FROM node:10.12.0-jessie

RUN groupadd -r app && useradd -rm -g app app

RUN mkdir -p /usr/src/
ADD frontend /usr/src/app
WORKDIR /usr/src/app

RUN yarn
RUN yarn add sqlite3
RUN yarn add pg
RUN yarn wp_build

RUN chown -R app:app /usr/src/app

USER app

EXPOSE 8080

CMD ["yarn", "run", "start_connect"]