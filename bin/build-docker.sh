#!/usr/bin/env bash

VERSION=1.0.1

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd "${DIR}/.."

docker build --build-arg NPM_TOKEN=${NPM_TOKEN} --build-arg NPM_USER=${NPM_USER} -t docker.atl-paas.net/atlassian/confluence-tree-search-connect-addon:${VERSION} .

#Use this command to run docker image propagating params from env
#docker run -p 3000:8080 -e NODE_ENV='local_docker' -e CTS_ADDON_NAME=\'$CTS_ADDON_NAME\' -e CTS_BASE_URL=\'$CTS_BASE_URL\' docker.atl-paas.net/atlassian/confluence-tree-search-connect-addon:${VERSION}


popd



