#!/usr/bin/env bash


SPACE_NAME=TESTE
PAGES_AMOUNT=200
CONFLUENCE_URL=https://kabakumov3.atlassian.net

if [ -z "$USER_NAME" ]; then
    echo "User name not set. Use USER_NAME env variable"
    exit 1
fi

if [ -z "$PASSWORD" ]; then
    echo "PASSWORD not set. Use PASSWORD env variable"
    exit 1
fi

for i in `seq 1 ${PAGES_AMOUNT}`;
do
  curl -u $USER_NAME:$PASSWORD -X POST -H 'Content-Type: application/json' -d "{\"type\":\"page\",\"title\":\"new page ${i}\",
\"space\":{\"key\":\"${SPACE_NAME}\"},\"body\":{\"storage\":{\"value\":\"<p>This is <br/> a new page ${i}</p>\",\"representation\":
\"storage\"}}}" "${$CONFLUENCE_URL}/wiki/rest/api/content/" | python -mjson.tool
done


