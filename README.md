Tree Search for Confluence
==========================

Source code of the [Tree Search for Confluence](https://marketplace.atlassian.com/apps/1219829/tree-search-for-confluence?hosting=cloud&tab=overview) app

Tree Search app is an alternative search page, which helps you to find information on Confluence Cloud instance.

It organizes search results in a tree view, which you can browse through and choose the page you need. Tree Search gives you a better context around search results and allows you to narrow down the search by the particular location or contributor.

Tree Search can also execute custom CQL to perform you queries in the advanced mode.

## Getting started

```bash
nvm use 10.9.0 #If you have another nvm version
yarn
```

## Build & Run

1. Start ngrok on port 3000

   ```bash
     ./ngrok http 3000 --region=au
   ```

   You'll get ngrok url.

2. Configure your environment. 

   Use environment variables to set parameters for add-on launch. Leave NODE_ENV=development, set CTS_BASE_URL to your add-on ngrok url. Set CTS_ADDON_NAME to your add-on suffix 

   ```bash
   export NODE_ENV=development
   export CTS_BASE_URL=https://david.au.ngrok.io
   export CTS_ADDON_NAME=david 
   ``` 

3. Open __frontend__ directory 
4. Launch from frontend directory (set NODE_ENV to the config name you want to continue with):

   ```bash
   yarn run start_connect_dev
   ```

5. Go to your Ngrok URL

   You should be redirected to add-on descriptor url (like _https://david.au.ngrok.io/atlassian-connect.json_)

   And get the following text:

    ```json
    {
      "key": "confluence-tree-search-add-on-david",
      "name": "Confluence Tree Search Add-on david",
      "description": "Provides page for convenient search in Confluence.",
      "version": "2.0",
      "baseUrl": "https://david.au.ngrok.io",
      "vendor": { "name": "Atlassian", "url": "http://www.atlassian.com" },
      "scopes": ["READ"],
      "lifecycle": { "installed": "/installed" },
      "authentication": { "type": "jwt" },
      "modules": {
        "generalPages": [
          {
            "key": "TreeSearch",
            "url": "/search/tree-search.html",
            "name": { "value": "Tree Search david" },
            "conditions": [{ "condition": "user_is_logged_in" }]
          }
        ]
      }
    }
    ```

6. Install service to your confluence instance. Go yo Manage Add-on section

   Confluence **Settings** -> **Manage Add-ons**

7. Click on "Settings" button in the page bottom and tick "Enable private listings" and "Enable development mode" flags

8. Press "Upload add-on" button and paste your add-on descriptor URL there.
   **Note:** Use https://your.ngrok.url/atlassian-connect.json Pure ngrok url will not work

9. Reload the page, after that you'll see Tree-Search link in a side-bar

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
