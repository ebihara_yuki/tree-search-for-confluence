const express = require("express");
const bodyParser = require("body-parser");
const compression = require("compression");
const cookieParser = require("cookie-parser");
const errorHandler = require("errorhandler");
const morgan = require("morgan");
// You need to load `atlassian-connect-express` to use her godly powers
const ac = require("atlassian-connect-express");
// Static expiry middleware to help serve static resources efficiently
process.env.PWD = process.env.PWD || process.cwd(); // Fix expiry on Windows :(
const expiry = require("static-expiry");

const http = require("http");
const path = require("path");
const os = require("os");

// Anything in ./public is served up as static content
const staticDir = path.join(__dirname, "public");

// Your routes live here; this is the C in MVC
const routes = require("./routes");
// Bootstrap Express
const app = express();
// Bootstrap the `atlassian-connect-express` library
const addon = ac(app, {
  config: {
    // enrich atlassian-connect.json with additional args
    descriptorTransformer: function(descriptor, config) {

      const suffix =  process.env.CTS_ADDON_NAME || (process.env.NODE_ENV !== "production" ? "development" : false);

      if (suffix && suffix !== '') {
        descriptor.key += `-${suffix}`;
        descriptor.name += ` ${suffix}`;
        descriptor.modules.generalPages[0].name.value += ` ${suffix}`;
      }

      return descriptor;
    }
  }
});
// You can set this in `config.json`
const port = addon.config.port();
// Declares the environment to use in `config.json`
const devEnv = app.get("env") !== "production";

app.use(function(req, res, next) {
  res.setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload')
  next();
});

// The following settings applies to all environments
app.set("port", port);

app.use(express.static(__dirname + "/dist"));
app.use("/search", express.static(__dirname + "/public"));

// Declare any Express [middleware](http://expressjs.com/api.html#middleware) you'd like to use here
// Log requests, using an appropriate formatter by env
app.use(morgan(devEnv ? "dev" : "combined"));
// Include request parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
// Gzip responses when appropriate
app.use(compression());

// You need to instantiate the `atlassian-connect-express` middleware in order to get its goodness for free
app.use(addon.middleware());
// Enable static resource fingerprinting for far future expires caching in production
app.use(expiry(app, { dir: staticDir, debug: devEnv }));
// // Add an hbs helper to fingerprint static resource urls
// hbs.registerHelper('furl', function(url){ return app.locals.furl(url); });
// Mount the static resource dir
app.use(express.static(staticDir));

// Show nicer errors when in dev mode
if (devEnv) app.use(errorHandler());

// Wire up your routes using the express and `atlassian-connect-express` objects
routes(app, addon);

const accesslog = require("access-log");
app.use(function(req, res, next) {
  accesslog(req, res);
  next();
});

// Boot the damn thing
http.createServer(app).listen(port, function() {
  console.log("Add-on server running at http://" + os.hostname() + ":" + port);
});
