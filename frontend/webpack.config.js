var path = require("path");

const outputDirectory = "dist";

const env = process.env.NODE_ENV || "development";

module.exports = {
  entry: ["whatwg-fetch", "./src/index.js"],
  output: {
    path: path.join(__dirname, outputDirectory),
    filename: "bundle.js"
  },
  mode: env === "production" ? "production" : "development",
  target: "web", // in order to ignore built-in modules like path, fs, etc.
  module: {
    rules: [
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        exclude: /node_modules/,
        use: [
          "file-loader?hash=sha512&digest=hex&name=[hash].[ext]",
          "image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false"
        ]
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ["react-hot-loader/webpack", "babel-loader"]
      }
    ]
  },
  resolve: {
    extensions: [".js", ".css"]
  },
  devServer: {
    port: 3000,
    host: "0.0.0.0",
    disableHostCheck: true,
    watchContentBase: true,
    contentBase: path.join(__dirname),
    proxy: {
      "/search": "http://localhost:8081",
      "/installed": "http://localhost:8081",
      "/atlassian-connect.json": "http://localhost:8081",
      "/internal/healthcheck": "http://localhost:8081",
      "/content-search.svg": "http://localhost:8081",
      "/privacy-statement.html": "http://localhost:8081",
      "/tenant-info": "http://localhost:8081"
    }
  }
};
