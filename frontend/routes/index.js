const https = require('https');
module.exports = function (app, addon) {
  app.get('/', function (req, res) {

    res.format({
      'text/html': function () {
        res.redirect('/atlassian-connect.json');
      },
      'application/json': function () {
        res.redirect('/atlassian-connect.json');
      }
    });
  });

  app.get('/tree-search', addon.authenticate(), function (req, res) {
    res.render('tree-search', {
      baseUrl: res.locals.hostBaseUrl,
    });
  });

  app.get('/internal/healthcheck', (req, res) => res.send('It works'));


  const ATLASSIAN_HOSTNAME_REGEXP = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*atlassian.net$/;

  app.get('/tenant-info', (req, res) => {

    const hostName = req.query.host;

    if (!ATLASSIAN_HOSTNAME_REGEXP.test(hostName)) {
      res.sendStatus(400);
      res.send("Invalid host parameter");
      return;
    }

    const options = {
      host: hostName,
      port: 443,
      path: '/_edge/tenant_info',
      method: 'GET'
    };

    const cloudIdRequest = https.request(options, function (responseFromTenant) {
      responseFromTenant.on("data", function (chunk) {
        console.log(`CloudID reffff respond with code ${responseFromTenant.statusCode} and content ${chunk}`);
        res.send(`${chunk}`);
      });

    }).on('error', err => {
      console.log("Error happened " + err);
      res.sendStatus(500);
      res.send("Error");
    });


    cloudIdRequest.end();
  });

};
