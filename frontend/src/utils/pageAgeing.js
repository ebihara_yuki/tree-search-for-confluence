const MILLIS_IN_MINUTE = 60 * 1000;
const MILLIS_IN_HOUR = 60 * MILLIS_IN_MINUTE;
const MILLIS_IN_DAY = 24 * MILLIS_IN_HOUR;
const MILLIS_IN_MONTH = 31 * MILLIS_IN_DAY;
const MILLIS_IN_YEAR = 365 * MILLIS_IN_DAY;


const OLDEST_OPACITY = 0.2; //Opacity of pages and nodes older than FINISH_AGE

const START_AGE = 3 * MILLIS_IN_MONTH; //Start age of page ageing. Opacity of that pages will be decreased progressively to OLDEST_OPACITY
const FINISH_AGE = 3 * MILLIS_IN_YEAR; //Finish age of page ageing. Pages older than that will have opacity = OLDEST_OPACITY


export function getOpacityByDate(time) {

  const current = new Date().getTime();

  const age = current - time;

  if (age > FINISH_AGE) {
    return OLDEST_OPACITY;
  }

  if (age < START_AGE) {
    return 1;
  }

  return 1 - ((1 - OLDEST_OPACITY) * (age - START_AGE)) / (FINISH_AGE - START_AGE);

}


const LOCAL_STORAGE_PAGE_AGEING_KEY = "confluence.tree.search.settings.pageAgeing";
const DEFAULT_PAGE_AGEING_SETTING = false;

export function savePageAgeingSetting(value) {
  localStorage.setItem(LOCAL_STORAGE_PAGE_AGEING_KEY, value ? "true" : "false");
}


export function getPageAgeingSetting() {
  const pageAgeingSetting = localStorage.getItem(LOCAL_STORAGE_PAGE_AGEING_KEY);
  return pageAgeingSetting == null ? DEFAULT_PAGE_AGEING_SETTING : (pageAgeingSetting === "true");
}