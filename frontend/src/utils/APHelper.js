export function confluenceRequest(url, type, successHandler, errorHandler) {
  window.AP.request({
    url: url,
    type: type,
    success: successHandler,
    error: errorHandler
  });
}


export function getConfluenceBaseUrl(callback) {
  window.AP.getLocation(function (location) {
    const url = new URL(location);
    callback("https://" + url.hostname + "/wiki");
  });
}


export function openConfluencePage(contentId) {
  window.AP.navigator.go('contentview', {contentId: contentId});
}