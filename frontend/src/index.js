// @flow
import React from "react";
import ReactDOM from "react-dom";
import HomePage from "./HomePage";
import TreeSearchPage from "./components/TreeSearchPage";
import "@atlaskit/css-reset";
import './css/global.css';

const root = document.getElementById("app-root");
if (root) {
  ReactDOM.render(<HomePage/>, root);
}

const iframeRoot = document.getElementById("iframe-root");
if (iframeRoot) {
  ReactDOM.render(<TreeSearchPage/>, iframeRoot);
}

