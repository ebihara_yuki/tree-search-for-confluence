import { akGridSize } from '@atlaskit/util-shared-styles';
const gridSizeInt = parseInt(akGridSize, 10);

export const CONFLUENCE_TREE_SEARCH_VERSION = '1.0.1';

export { gridSizeInt };
