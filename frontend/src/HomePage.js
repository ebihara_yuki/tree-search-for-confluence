// @flow
import React, { PureComponent } from "react";
import styled from "styled-components";
import TreeSearchPage from "./components/TreeSearchPage";

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  position: absolute;
  min-height: 100%;
  min-width: 100%;
`;

export default class HomePage extends PureComponent<{}> {
  render() {
    return (
      <Wrapper>
        <TreeSearchPage />
      </Wrapper>
    );
  }
}
