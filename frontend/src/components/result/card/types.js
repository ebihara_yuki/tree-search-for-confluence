// @flow

import type { Contributor } from "../../../search/model/DPathElement";

export type ResultCardType = {|
  excerpt: string,
  friendlyLastModified: string,
  id: string,
  links: { self: string, tinyui: string, editui: string, webui: string },
  timestamp: number,
  title: string,
  type: string,
  confluenceUrl: string,
  contributors: Contributor[],
  count: number
|};
