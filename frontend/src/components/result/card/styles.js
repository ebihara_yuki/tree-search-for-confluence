// @flow

import styled from "styled-components";
import { gridSize, textColor, fontSize } from "../../common/styles";
import { colors } from "@atlaskit/theme";

export const CardTitleWrapper = styled.div`
  display: flex;
  align-items: flex-start;
`;
export const Card = styled.div`
  display: block;
  white-space: normal;
  color: ${textColor};
  margin-top: ${gridSize / 2}px;
  padding: 0 ${gridSize}px;
`;

export const CardTitle = styled.a`
  font-size: ${fontSize}px;
  font-weight: 600;
  color: ${colors.B400};
  margin-left: ${gridSize}px;
  margin-right: ${gridSize / 2}px;
  margin-top: ${gridSize / 4}px;
`;

export const CardDesc = styled.div`
  font-size: 12px;
  margin: ${gridSize / 2}px 0;
`;

export const PageIcon = styled.div`
  margin-top: ${gridSize / 4}px;
`;
