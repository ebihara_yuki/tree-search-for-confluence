// @flow

import React, {Fragment, PureComponent} from "react";
import {type ResultCardType} from "./types";
import {Card, CardDesc, CardTitle, CardTitleWrapper, PageIcon} from "./styles";
import Page16Icon from "@atlaskit/icon-object/glyph/page/16";
import Badge from "@atlaskit/badge";
import AvatarGroup from "@atlaskit/avatar-group";
import {CONTRIBUTOR_SEARCH} from "../../common/constants";
import _ from 'lodash';

const LINE_HEIGHT: number = 20;
const LINES: number = 2;

type Props = {
  data: ResultCardType,
  isExpanded: boolean,
  hasChildren: boolean,
  // TODO: add filter type
  onAddFilter: (filter: any) => void
};

export class ResultCard extends PureComponent<Props> {
  static defaultProps = {
    isExpanded: true
  };

  transformDescription = (text: string) => {
    text = text || "";

    const formmattedText = _.escape(text)
      .replace(/@@@hl@@@/g, "<strong>")
      .replace(/@@@endhl@@@/g, "</strong>");

    return formmattedText;
  };

  onAvatarClick = ({item}) => {
    const {onAddFilter} = this.props;
    onAddFilter({
      label: item.name,
      value: item.accountId,
      iconUrl: item.src,
      type: CONTRIBUTOR_SEARCH
    });
  };

  calculateDescWidth = (div: HTMLDivElement | null) => {
    if (div) {
      const {data} = this.props;
      const {excerpt} = data;
      let text: string;
      div.innerHTML = this.transformDescription(excerpt);
      while (div.offsetHeight > LINES * LINE_HEIGHT) {
        text = div.innerHTML.trim();
        if (text.split(" ").length <= 1) {
          break;
        }
        div.innerHTML = text.replace(/\W*\s(\S)*$/, "...");
      }
    }
  };

  render() {
    const {data, isExpanded, hasChildren} = this.props;
    const {title, confluenceUrl, links, contributors, count} = data;

    const avatarData = contributors.map(contributor => {
      return {
        name: contributor.displayName,
        accountId: contributor.accountId,
        src: `${confluenceUrl}${contributor.profilePicture.path.replace(
          /^\/wiki/,
          ""
        )}`,
        appearance: "circle",
        size: "medium",
        enableTooltip: true
      };
    });

    const resultUrl = confluenceUrl + links.webui;


    return (
      <Card>
        <CardTitleWrapper>
          <PageIcon>
            <Page16Icon/>
          </PageIcon>
          <CardTitle
            target="_parent"
            href={resultUrl}
            dangerouslySetInnerHTML={{
              __html: this.transformDescription(title)
            }}
          />
          {count > 1 && (
            <div>
              <Badge>{count}</Badge>
            </div>
          )}
        </CardTitleWrapper>
        {isExpanded && (
          <Fragment>
            <CardDesc innerRef={this.calculateDescWidth}/>
            <AvatarGroup
              appearance="stack"
              onAvatarClick={this.onAvatarClick}
              data={avatarData}
              size="small"
            />
          </Fragment>
        )}
      </Card>
    );
  }
}

export default ResultCard;
