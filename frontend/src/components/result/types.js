// @flow

import type { DPathElement } from "../../search/model/DPathElement";
import type { DSingleSearchResult } from "../../search/model/DSingleSearchResult";

export type TreeNode = {
  path: DPathElement[],
  hasChildren: boolean,
  children: TreeNode[],
  result: DSingleSearchResult | void,
  rank: number,
  lastModificationTimestamp: number,
  count: number
};
