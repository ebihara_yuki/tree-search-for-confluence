// @flow

import styled from "styled-components";
import { colors } from "@atlaskit/theme";
import { gridSize } from "../common/styles";

export const CellItem = styled.div`
  text-align: center;

  ${props =>
    props.textAlign &&
    `text-align: ${props.textAlign};   
    `};
  ${props =>
    props.width &&
    `width: ${props.width}px;   
    `};
`;

export const SpaceWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  border: 2px solid transparent;
`;

export const SearchResultsContainer = styled.div`
  margin-top: 10px;

  [role="row"] {
    border-bottom: 1px solid transparent;
  }

  [role="treegrid"] {
    > [role="row"] {
      border-bottom: 1px solid ${colors.N40};
    }
  }

  [role="columnheader"] {
    padding-left: ${gridSize / 2}px;
    padding-right: ${gridSize / 2}px;
    justify-content: center;
  }

  [role="columnheader"]:first-child {
    justify-content: flex-start;
  }

  button[aria-controls] {
    margin-top: ${gridSize / 2}px;
  }
`;

export const ButtonGroupContainer = styled.div`
  margin-bottom: ${gridSize}px;
`;

export const Link = styled.a`
  margin: ${gridSize / 2}px;
  color: ${colors.N200};
  cursor: pointer;
  font-size: 12px;
  &:hover {
    color: ${colors.N200};
  }
`;
