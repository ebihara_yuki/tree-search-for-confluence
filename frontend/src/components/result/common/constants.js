// @flow

export const COLUMN_SIDE_PADDING: number = 25;
export const RANKED_COL_WIDTH: number = 90;
export const UPDATED_COL_WIDTH: number = 160;
export const REFINED_COL_WIDTH: number = 200;
