// @flow

import { colors } from "@atlaskit/theme";
import { gridSize } from "../../common/styles";

import styled from "styled-components";

export const IconContainer = styled.div`
  margin-right: ${gridSize}px;

  path {
    fill: ${colors.N70} !important;
  }
`;

export const BreadcrumbsContainer = styled.div`
  margin-top: ${gridSize / 4}px;
`;

export const BadgeContainer = styled.div`
  margin-top: ${gridSize / 4}px;
`;
