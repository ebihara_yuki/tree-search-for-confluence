import type {IntlShape} from "react-intl";
import React, { Component} from "react";
import Button from "@atlaskit/button";
import SearchIcon from "@atlaskit/icon/glyph/search";


type Props = {
  isDisabled: boolean,
  text: string,
  onClick: () => void
};


export default class SearchHereButton extends Component<Props> {

  shouldComponentUpdate(prevProps) {

    if (
      prevProps.isDisabled === this.props.isDisabled &&
      prevProps.text === this.props.text
    ) {
      return false;
    }
    return true;
  }


  render() {
    return <Button
      isDisabled={this.props.isDisabled}
      appearance="default"
      onClick={this.props.onClick}
      iconBefore={<SearchIcon size="small"/>}
    >
      {this.props.text}
    </Button>
  }


}
