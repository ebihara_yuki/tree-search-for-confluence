// @flow

import { defineMessages } from "react-intl";

export default defineMessages({
  searchResult: {
    id: "confluencetreesearch.result.tabletree.searchResult",
    defaultMessage: "Search results",
    description: ""
  },
  ranked: {
    id: "confluencetreesearch.result.tabletree.ranked",
    defaultMessage: "Ranked",
    description: ""
  },
  updated: {
    id: "confluencetreesearch.result.tabletree.updatedRecently",
    defaultMessage: "Updated recently",
    description: ""
  },
  refined: {
    id: "confluencetreesearch.result.tabletree.searchResult",
    defaultMessage: "Refined search",
    description: ""
  },
  addFilter: {
    id: "confluencetreesearch.result.tabletree.addFilter",
    defaultMessage: "Search here",
    description: ""
  }
});
