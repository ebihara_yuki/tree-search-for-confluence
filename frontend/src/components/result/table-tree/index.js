// @flow

import React, {PureComponent} from "react";
import {injectIntl, type IntlShape} from "react-intl";
import Page16Icon from "@atlaskit/icon-object/glyph/page/16";
import SearchHereButton from './search-here-button';
import Badge from "@atlaskit/badge";
import TableTree, {Cell, Header, Headers, Row, Rows} from "@atlaskit/table-tree";
import {COLUMN_SIDE_PADDING, RANKED_COL_WIDTH, REFINED_COL_WIDTH, UPDATED_COL_WIDTH} from "../common/constants";
import {type TreeNode} from "../types";
import Avatar from "@atlaskit/avatar";
import Breadcrumbs, {BreadcrumbsItem} from "@atlaskit/breadcrumbs";
import {ResultCard} from "../../result";
import {CellItem, SpaceWrapper} from "../styles";
import format from "date-fns/format";
import {type Option, PAGE_SEARCH, SPACE_SEARCH} from "../../common/constants";
import messages from "./messages";
import {BadgeContainer, BreadcrumbsContainer, IconContainer} from "./styled";
import type {DPathElement} from "../../../search/model/DPathElement";
import {FilterContext} from "../../search/form";

type Props = {
  tableTreeData: TreeNode[],
  confluenceUrl: string,
  addFilter: () => void,
  isDefaultExpanded: boolean,
  columnWidth: number,
  intl: IntlShape
  // searchText: string
};

const displayRegExp = /^\/display\/(.*)/;
const spaceRegExp = /^\/spaces\/(.*)\/overview/;
const pageRegExp = /^\/spaces\/(.*)\/pages\/(\d+)/;

export class ResultTableTree extends PureComponent<Props> {
  buildFilterDataFromLink = (path: DPathElement): Option => {
    const link = path.link;

    if (pageRegExp.test(link)) {
      const [, , pageId] = pageRegExp.exec(link);

      return {
        value: Number(pageId),
        type: PAGE_SEARCH,
        iconUrl: path.icon,
        label: path.title
      };
    }
    if (spaceRegExp.test(link)) {
      const [, space] = spaceRegExp.exec(link);

      return {
        value: space,
        type: SPACE_SEARCH,
        iconUrl: path.icon,
        label: path.title
      };
    }
    if (displayRegExp.test(link)) {
      const [, space] = displayRegExp.exec(link);

      return {
        value: space,
        type: SPACE_SEARCH,
        iconUrl: path.icon,
        label: path.title
      };
    }

    return null;
  };

  isFilterSelected = (filterData: Option, filterProps): boolean => {
    const {searchFilters, searchText} = filterProps;
    if (!searchFilters) {
      return false;
    }
    const found = searchFilters.filter(fd => fd.value === filterData.value);
    return (
      found.length > 0 || searchText.indexOf(filterData.value.toString()) > -1
    );
  };

  getIcon = (paths: DPathElement[]) => {
    if (!paths || paths.length === 0 || !(paths && paths[0].icon)) {
      return null;
    }

    const {confluenceUrl} = this.props;
    return `${confluenceUrl}${paths[0].icon.path}`;
  };

  getFilterData = (paths: DPathElement[], iconUrl: string) => {
    return this.buildFilterDataFromLink({
      ...paths[paths.length - 1],
      icon: iconUrl ? iconUrl : undefined
    });
  };

  render() {
    const {
      tableTreeData,
      confluenceUrl,
      addFilter,
      columnWidth,
      isDefaultExpanded,
      intl: {formatMessage}
    } = this.props;

    return (

      <TableTree>
        <Headers>
          <Header width={columnWidth}>
            {formatMessage(messages.searchResult)}
          </Header>
          <Header width={RANKED_COL_WIDTH}>
            {formatMessage(messages.ranked)}
          </Header>
          <Header width={UPDATED_COL_WIDTH}>
            {formatMessage(messages.updated)}
          </Header>
          <Header width={REFINED_COL_WIDTH}>
            {formatMessage(messages.refined)}
          </Header>
        </Headers>
        <Rows
          items={tableTreeData}
          render={({
                     id,
                     path,
                     rank,
                     count,
                     lastModificationTimestamp,
                     hasChildren,
                     children,
                     result
                   }) => {
            const icon = this.getIcon(path);
            const avatar = icon ? (
              <Avatar appearance="square" src={icon} size="small"/>
            ) : null;

            const filterData: Option = this.getFilterData(path, icon || "");
            const breadcrumbs = result
              ? path.slice(0, path.length - 1)
              : path;

            return (
              <Row
                expandLabel={"Expand"}
                collapseLabel={"Collapse"}
                itemId={id}
                items={children}
                hasChildren={hasChildren}
                isDefaultExpanded={isDefaultExpanded}
              >
                <Cell singleLine>
                  {breadcrumbs.length > 0 ? (
                    <SpaceWrapper>
                      {avatar}
                      <BreadcrumbsContainer>
                        <Breadcrumbs>
                          {breadcrumbs.map((pathItem, index) => {
                            return (
                              <BreadcrumbsItem
                                key={pathItem.id}
                                text={pathItem.title}
                                href={`${confluenceUrl}${pathItem.link}`}
                                //That's a temporal stub. To properly aviod iframe inside iframe we need to use AJS lib (openConfluencePage in the AP Helper class)
                                target="_blank"
                                iconBefore={
                                  !avatar &&
                                  index === 0 && (
                                    <IconContainer>
                                      <Page16Icon/>
                                    </IconContainer>
                                  )
                                }
                              />
                            );
                          })}
                        </Breadcrumbs>
                      </BreadcrumbsContainer>
                      <BadgeContainer>
                        <Badge>{count}</Badge>
                      </BadgeContainer>
                    </SpaceWrapper>
                  ) : null}

                  {result && (
                    <ResultCard
                      data={{
                        ...result,
                        confluenceUrl,
                        hasChildren,
                        count
                      }}
                      onAddFilter={addFilter}
                    />
                  )}
                </Cell>
                <Cell>
                  <CellItem
                    width={RANKED_COL_WIDTH - COLUMN_SIDE_PADDING * 2}
                  >
                    {String(rank + 1)}
                  </CellItem>
                </Cell>
                <Cell>
                  <CellItem
                    width={UPDATED_COL_WIDTH - COLUMN_SIDE_PADDING * 2}
                  >
                    {format(
                      new Date(lastModificationTimestamp),
                      "DD/MM/YYYY"
                    )}
                  </CellItem>
                </Cell>
                <Cell>
                  <CellItem
                    width={REFINED_COL_WIDTH - COLUMN_SIDE_PADDING * 2}
                  >
                    {filterData && hasChildren ? (
                      this.makeFilterButton(filterData, addFilter, formatMessage)
                    ) : null}
                  </CellItem>
                </Cell>
              </Row>
            );
          }}
        />
      </TableTree>

    );
  }

  makeFilterButton(filterData, addFilter, formatMessage) {
    return <FilterContext.Consumer>
      {filterProps => (<SearchHereButton
        isDisabled={this.isFilterSelected(
          filterData,
          filterProps
        )}
        onClick={() => {
          addFilter(filterData);
        }}
        text={formatMessage(messages.addFilter)}
      />)}
    </FilterContext.Consumer>;
  }
}

export default injectIntl(ResultTableTree);
