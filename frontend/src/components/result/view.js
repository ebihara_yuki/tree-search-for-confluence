// @flow

import React, {Component} from "react";
import type {DSearchResultTreeNode} from "../../search/model/DSearchResultTreeNode";
import type {DPathElement} from "../../search/model/DPathElement";

import {ButtonGroupContainer, Link, SearchResultsContainer} from "./styles";
import {type TreeNode} from "./types";
import {AMOUNT_OF_RESULT} from "../common/constants";
import {RANKED_COL_WIDTH, REFINED_COL_WIDTH, UPDATED_COL_WIDTH} from "./common/constants";
import ResultTableTree from "./table-tree";

type Props = {
  orderBy: Option,
  confluenceUrl: string,
  tree: DSearchResultTreeNode,
  addFilter: () => void,
  isDefaultExpanded: boolean
};

type State = {
  isDefaultExpanded: boolean,
  columnWidth: number
};

export class SearchResults extends Component<Props, State> {
  static defaultProps = {
    confluenceUrl: ""
  };
  state = {
    // start expanded and allow user to collapse
    isDefaultExpanded: this.props.isDefaultExpanded,
    columnWidth: 0
  };

  componentDidMount() {
    window.addEventListener("resize", this.onResize, false);
  }

  shouldComponentUpdate(prevProps: Props, prevState: State) {
    /**
     * To prevent table reordering when orderBy changed to AMOUNT_OF_RESULT.
     * It should only reorder/render after Search button is clicked.
     */
    if (
      prevState.isDefaultExpanded === this.state.isDefaultExpanded &&
      prevState.columnWidth === this.state.columnWidth &&
      prevProps.tree === this.props.tree &&
      prevProps.orderBy === this.props.orderBy
    ) {
      return false;
    }
    return true;
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize);
  }

  onResize = () => {
    this.tableRef && this.calculateWrapperWidth(this.tableRef);
  };

  setDefaultExpanded = (isDefaultExpanded: boolean) => {
    this.setState({
      isDefaultExpanded
    });
  };

  sortResultByChildrenCount = (treeNode: TreeNode[]) => {
    return treeNode.sort((a, b) => {
      return a.count < b.count ? 1 : a.count > b.count ? -1 : 0;
    });
  };

  transformTree(orderByAmount: boolean): TreeNode[] {
    const {tree} = this.props;

    const traverse = (node: DSearchResultTreeNode,
                      path: DPathElement[]): TreeNode => {
      // if there is only child, we merge nodes
      if (node.children.length === 1 && !node.children[0].result) {
        const child = node.children[0];

        // add new element to the path and jump to the child
        return traverse(child, [...path, node.pathElement]);
      }
      const Node = {
        id: node.id,
        path: [...path, node.pathElement],
        hasChildren: node.children.length > 0,
        children: node.children.map(child => traverse(child, [])),
        result: node.result ? node.result : undefined,
        rank: node.rank,
        count: node.count,
        lastModificationTimestamp: node.lastModificationTimestamp
      };
      if (orderByAmount) {
        Node.children = this.sortResultByChildrenCount(Node.children);
      }
      return Node;
    };

    const treeResult = tree.children.map(child => traverse(child, []));

    return orderByAmount
      ? this.sortResultByChildrenCount(treeResult)
      : treeResult;
  }

  tableRef: HTMLDivElement | null;
  calculateWrapperWidth = (div: HTMLDivElement | null) => {
    if (div) {
      let pageWidth = div.offsetWidth;
      const columns = [RANKED_COL_WIDTH, UPDATED_COL_WIDTH, REFINED_COL_WIDTH];
      for (let i = 0; i < columns.length; i++) {
        pageWidth = pageWidth - columns[i];
      }
      if (this.tableRef !== div) {
        this.tableRef = div;
      }
      this.setState({columnWidth: pageWidth});
    }
  };

  collapseAllAction = () => {
    this.setDefaultExpanded(false);
  };

  expandAllAction = () => {
    this.setDefaultExpanded(true);
  };

  render() {
    const {confluenceUrl, addFilter, orderBy} = this.props;
    const {columnWidth, isDefaultExpanded} = this.state;

    const orderByAmount: boolean = orderBy === AMOUNT_OF_RESULT;
    const tableTree = this.transformTree(orderByAmount);
    return (
      <SearchResultsContainer innerRef={this.calculateWrapperWidth}>
        <ButtonGroupContainer>
          {isDefaultExpanded ? (
            <Link onClick={this.collapseAllAction}>Collapse All</Link>
          ) : (
            <Link onClick={this.expandAllAction}>Expand All</Link>
          )}
        </ButtonGroupContainer>
        <ResultTableTree
          tableTreeData={tableTree}
          confluenceUrl={confluenceUrl}
          addFilter={addFilter}
          columnWidth={columnWidth}
          isDefaultExpanded={isDefaultExpanded}
        />
      </SearchResultsContainer>
    );
  }
}

export default SearchResults;
