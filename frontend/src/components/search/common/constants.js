// @flow

export const NOT_STARTED = "NOT_STARTED";
export const LOADING = "LOADING";
export const SUCCESS = "SUCCESS";
export const ERROR = "ERROR";
export type LoadingStatus = NOT_STARTED | LOADING | SUCCESS | ERROR;

export const SEARCH_MODE_CQL = "CQL";
export const SEARCH_MODE_NORMAL = "NORMAL";
export const CQL_HELP_LINK =
  "https://confluence.atlassian.com/confcloud/confluence-search-syntax-724765423.html";
