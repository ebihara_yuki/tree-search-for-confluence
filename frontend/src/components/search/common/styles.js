// @flow

import { gridSize, textColor } from "../../common/styles";
import { colors } from "@atlaskit/theme";
export const SearchFieldBgColor = colors.N30;

export const getCustomStyles = {
  control: (base, state) => ({
    ...base,
    backgroundColor: state.isFocused ? `${SearchFieldBgColor}` : "transparent",
    color: `${textColor}`
  }),
  singleValue: (base, state) => ({
    ...base,
    color: `${textColor}`,
    position: `relative`,
    transform: `none`,
    top: `auto`
  }),
  multiValue: (base, state) => ({
    ...base,
    color: `${textColor}`,
    backgroundColor: "transparent"
  }),
  multiValueLabel: (base, state) => ({
    ...base,
    fontSize: "14px",
    padding: `0 ${gridSize / 2}px`,
    backgroundColor: "transparent"
  }),
  multiValueRemove: (base, state) => ({
    ...base,
    color: `${colors.N70}`,
    ":hover": {
      backgroundColor: `${colors.N40}`
    }
  }),
  valueContainer: (base, state) => ({
    ...base,
    flexWrap: `nowrap`,
    ":hover": {
      border: `none`
    }
  })
};
