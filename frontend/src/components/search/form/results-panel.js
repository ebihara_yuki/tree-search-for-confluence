import React, { Component} from "react";

import type {Option} from "../../common/constants";
import {CQL_HELP_LINK, ERROR, LOADING, NOT_STARTED} from "../common/constants";
import type {LoadingStatus} from "../common/constants";
import type {DTreeSearchResult} from "../../../search/model/DTreeSearchResult";
import QuestionCircleIcon from "@atlaskit/icon/glyph/question-circle";
import {ErrorText, SpinnerWrapper} from "./styles";
import Spinner from "@atlaskit/spinner";
import messages from "./messages";
import {Result} from "../../result";
import type {IntlShape} from "react-intl";
import {injectIntl} from "react-intl";


type Props = {
  intl: IntlShape,
  searchResult: DTreeSearchResult | void,
  loadingStatus: LoadingStatus,
  errorText: string,
  orderBy: string,
  baseUrl: string,
  isDefaultExpanded: boolean,
  onAddFilter: (Option) => void
};

class ResultsPanel extends Component<Props> {


  shouldComponentUpdate(prevProps) {

    if (
      prevProps.searchResult === this.props.searchResult &&
      prevProps.loadingStatus === this.props.loadingStatus &&
      prevProps.errorText === this.props.errorText &&
      prevProps.isDefaultExpanded === this.props.isDefaultExpanded &&
      prevProps.orderBy === this.props.orderBy
    ) {
      return false;
    }
    return true;
  }

  render() {

    const {
      intl: {formatMessage},
      searchResult,
      loadingStatus,
      errorText,
      orderBy,
      baseUrl,
      isDefaultExpanded,
      onAddFilter
    } = this.props;

    if (loadingStatus === NOT_STARTED) return null;

    if (loadingStatus === LOADING && !searchResult) {
      return (
        <SpinnerWrapper>
          <Spinner/>
        </SpinnerWrapper>
      );
    }

    if (loadingStatus === ERROR) {

      return (
        <ErrorText>
          {errorText && errorText !== ""
            ? errorText
            : formatMessage(messages.error)}
          <a href={CQL_HELP_LINK} target="_blank" title="Syntax Help">
            <QuestionCircleIcon size="small" label="Syntax Help"/>
          </a>
        </ErrorText>
      );
    }

    if (searchResult) {
      const resultsElement = (
        <Result
          tree={searchResult.tree}
          orderBy={orderBy}
          confluenceUrl={baseUrl}
          addFilter={onAddFilter}
          isDefaultExpanded={isDefaultExpanded}
        />
      );

      return (
        <section>
          {loadingStatus === LOADING && (
            <SpinnerWrapper>
              <Spinner/>
            </SpinnerWrapper>
          )}
          {resultsElement}
        </section>
      );
    }
    return null;
  };
}

export default injectIntl(ResultsPanel);