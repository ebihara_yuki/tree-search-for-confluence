// @flow

import React, { Component } from "react";
import { injectIntl, type IntlShape } from "react-intl";
import {
  FilterWrapper,
  FilterLabel,
  FilterDropdownWrapperWide,
  FilterDropdownWrapper,
  FilterControls,
  FilterButtons
} from "./styles";

import { getCustomStyles } from "../../common/styles";
import { amountOptions, orderOptions } from "../../../common/constants";
import { SEARCH_MODE_NORMAL } from "../../common/constants";
import messages from "./messages";
import Button from "@atlaskit/button";
import Select from "@atlaskit/select";

type Props = {
  intl: IntlShape,
  searchMode: string,
  onToggleSearchMode: () => void,
  onSearch: () => void,
  orderBy: string,
  maxResults: string,
  onOrderByChange: () => void,
  onAmountChange: () => void
};
export class SearchControls extends Component<Props> {
  shouldComponentUpdate(prevProps : Props) {
    if (
      prevProps.orderBy === this.props.orderBy &&
      prevProps.maxResults === this.props.maxResults &&
      prevProps.searchMode === this.props.searchMode
    ) {
      return false;
    }

    return true;
  }
  renderFilterControls() {
    const {
      intl: { formatMessage },
      orderBy,
      maxResults,
      onOrderByChange,
      onAmountChange
    } = this.props;

    const selectedOrder = orderBy
      ? orderOptions.filter(ob => orderBy === ob.value)[0]
      : orderOptions[0];

    const selectedAmount = maxResults
      ? amountOptions.filter(mr => maxResults === mr.value)[0]
      : amountOptions[0];

    return (
      <FilterControls>
        <FilterLabel>{formatMessage(messages.orderby)}</FilterLabel>
        <FilterDropdownWrapperWide>
          <Select
            isSearchable={false}
            options={orderOptions}
            onChange={onOrderByChange}
            value={selectedOrder}
            styles={getCustomStyles}
          />
        </FilterDropdownWrapperWide>
        <FilterLabel>{formatMessage(messages.returnResults)}</FilterLabel>
        <FilterDropdownWrapper>
          <Select
            isSearchable={false}
            options={amountOptions}
            onChange={onAmountChange}
            value={selectedAmount}
            styles={getCustomStyles}
          />
        </FilterDropdownWrapper>
        {/* <CheckboxStateless
          isChecked={isDefaultExpanded}
          label={formatMessage(messages.displayResults)}
          name="displayResults"
          value="true"
          onChange={onDisplayModeChange}
        /> */}
      </FilterControls>
    );
  }

  render() {
    const {
      intl: { formatMessage },
      searchMode,
      onSearch,
      onToggleSearchMode
    } = this.props;

    return (
      <FilterWrapper searchMode={searchMode}>
        {searchMode === SEARCH_MODE_NORMAL ? this.renderFilterControls() : null}
        <FilterButtons>
          <Button appearance="primary" onClick={onSearch}>
            {formatMessage(messages.search)}
          </Button>
          <Button appearance="link" onClick={onToggleSearchMode}>
            {searchMode === SEARCH_MODE_NORMAL
              ? formatMessage(messages.advanced)
              : formatMessage(messages.basic)}
          </Button>
        </FilterButtons>
      </FilterWrapper>
    );
  }
}

export default injectIntl(SearchControls);
