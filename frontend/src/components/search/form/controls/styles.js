// @flow

import styled from "styled-components";
import {
  gridSize,
  fontFamily,
  fontSize,
  textColor
} from "../../../common/styles";

export const FilterWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: ${gridSize}px;
`;

export const FilterLabel = styled.div`
  font-family: ${fontFamily};
  font-size: ${fontSize}px;
  height: inherit;
  letter-spacing: normal;
  margin: 0 ${gridSize / 2}px;
  color: ${textColor};
`;
export const FilterDropdownWrapperWide = styled.div`
  width: 172px;
  margin: 0 ${gridSize / 2}px;
`;
export const FilterDropdownWrapper = styled.div`
  width: 90px;
  margin: 0 ${gridSize / 2}px;
`;

export const FilterControls = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const FilterButtons = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  margin-left: ${gridSize * 2}px;
`;
