// @flow

import { defineMessages } from "react-intl";

export default defineMessages({
  orderby: {
    id: "confluencetreesearch.search.form.orderby",
    defaultMessage: "Order By",
    description: ""
  },
  returnResults: {
    id: "confluencetreesearch.search.form.returnResults",
    defaultMessage: "Return Results",
    description: ""
  },
  search: {
    id: "confluencetreesearch.search.form.search",
    defaultMessage: "Search",
    description: ""
  },
  advanced: {
    id: "confluencetreesearch.search.form.advanced",
    defaultMessage: "Advanced",
    description: ""
  },
  basic: {
    id: "confluencetreesearch.search.form.basic",
    defaultMessage: "Basic",
    description: ""
  },
  displayResults: {
    id: "confluencetreesearch.search.form.displayResults",
    defaultMessage: "Expand results",
    description: ""
  }
});
