// @flow

import qs from "query-string";
import type { Option } from "../../common/constants";

import { SEARCH_MODE_CQL, SEARCH_MODE_NORMAL } from "../common/constants";

export type Query =
  | {|
      filters: Option[],
      cqlQuery: string,
      mode: typeof SEARCH_MODE_CQL
    |}
  | {|
      filters: Option[],
      text: string,
      mode: typeof SEARCH_MODE_NORMAL
    |};

export type ChangeEvent = {
  oldUrl: string,
  newUrl: string
};

export type Listener = (event: ChangeEvent) => void;

const history = window.AP.history;

export default class History {
  constructor() {
    this.listeners = [];

    history.popState((event: ChangeEvent) => {
      this.listeners.forEach(listener => {
        listener(event);
      });
    });
  }

  addListener(listener: Listener) {
    this.listeners.push(listener);
  }

  removeListener(listener: Listener) {
    this.listeners = this.listeners.filter(
      registeredListener => registeredListener !== listener
    );
  }

  pushQuery(query: Query) {
    history.pushState(
      qs.stringify({
        text: query.text,
        filters: query.filters.length
          ? JSON.stringify(query.filters)
          : undefined,
        cqlQuery: query.cqlQuery,
        mode: query.mode
      })
    );
  }

  getQuery(): Query {
    const query = qs.parse(history.getState());
    let filters = [];
    const searchMode = query.mode || SEARCH_MODE_NORMAL;
    const text = query.text || "";
    const cqlQuery = query.cqlQuery || "";

    try {
      filters = JSON.parse(query.filters);
    } catch (err) {
      filters = [];
    }

    if (searchMode === SEARCH_MODE_NORMAL) {
      return {
        mode: searchMode,
        text,
        filters
      };
    }

    return {
      mode: searchMode,
      cqlQuery,
      filters
    };
  }
}
