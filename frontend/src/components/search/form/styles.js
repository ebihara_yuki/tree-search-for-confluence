// @flow

import styled from "styled-components";
import { gridSize, textColor, borderRadius } from "../../common/styles";
import { colors } from "@atlaskit/theme";

export const FormWrapper = styled.div`
  display: flex;
  margin-top: ${gridSize * 2}px;
  background: ${colors.N20A};
  padding: ${gridSize * 2}px;
  flex-wrap: nowrap;
  justify-content: space-between;
  border-radius: ${borderRadius}px;
`;

export const SpinnerWrapper = styled.div`
  margin: ${gridSize * 2}px ${gridSize}px;
`;

export const ErrorText = styled.p`
  color: ${textColor};
  a,
  a:hover,
  a:focus {
    color: ${textColor};
    outline: 0;
    margin-left: ${gridSize / 2}px;
  }
`;
