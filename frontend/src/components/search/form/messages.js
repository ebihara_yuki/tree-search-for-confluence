// @flow

import { defineMessages } from "react-intl";

export default defineMessages({
  placeholderNormal: {
    id: "confluencetreesearch.search.form.placeholder",
    defaultMessage: "Search...",
    description: ""
  },
  placeholderCql: {
    id: "confluencetreesearch.search.form.placeholder",
    defaultMessage: "CQL search",
    description: ""
  },
  error: {
    id: "confluencetreesearch.search.form.error",
    defaultMessage: "Error loading results.",
    description: ""
  },
  cqlErrorTitle: {
    id: "confluencetreesearch.search.form.cqlErrorTitle",
    defaultMessage: "Oops, something went wrong!",
    description: ""
  },
  cqlErrorDescription: {
    id: "confluencetreesearch.search.form.cqlErrorDescription",
    defaultMessage:
      "Your search did not return any data. Please try again with a valid CQL syntax.",
    description: ""
  }
});
