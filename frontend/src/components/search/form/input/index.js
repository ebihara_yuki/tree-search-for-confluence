// @flow

import React, { Fragment, PureComponent } from "react";
import Avatar from "@atlaskit/avatar";
import Tag from "@atlaskit/tag";
import Tooltip from "@atlaskit/tooltip";
import SearchIcon from "@atlaskit/icon/glyph/search";

import {
  SearchField,
  SearchFieldInput,
  SearchIconWrapper,
  TagWrapper
} from "./styles";
import { components, CreatableSelect } from "@atlaskit/select";
import { Option } from "../../../common/constants";
import { getCustomStyles } from "../../common/styles";

import { KEY_ENTER } from "../../../common/utils";

type Props = {
  tags: Option[],
  inputText: string,
  isMultiValue: boolean,
  placeholderText: string,
  baseUrl: string,
  onEnterPressed: void => void,
  onInputChange: object => void,
  onTagsChange: (Option[]) => void
};

const DropdownIndicator = props => (
  <SearchIconWrapper>
    <SearchIcon />
  </SearchIconWrapper>
);

/**
 * Test input component with search options tags.
 * It allows to enter search text and displays set of search tags which can be either user or ancestor.
 *
 * This is a controlled component and does not have it's own state.
 */
export default class SearchInput extends PureComponent<Props> {
  inputRef: HTMLElement | void | null; //We need to keep reference on the input element to refocus on it during the editing.
  //React will recreate element because of the props update, so we need to focus on the newly created input after typing each character.
  //TODO Maybe we do something wrong with elements tree/order so react have to re-create them again

  inputCursorPosition = 0;

  handleInputChangeEvent = event => {
    this.inputCursorPosition = this.inputRef.selectionStart;
    this.props.onInputChange(event.target.value);
  };

  onKeyDown = (event: SyntheticKeyboardEvent<>) => {
    event.stopPropagation();
    if (event.keyCode === KEY_ENTER) {
      this.props.onEnterPressed();
    }
  };

  onRemoveTag = (tag: Option) => {
    const tags = [...this.props.tags];
    const index = tags.indexOf(tag);
    tags.splice(index, 1);
    this.props.onTagsChange(tags);
  };

  componentDidUpdate(prevProps :Props) {
    if (prevProps && this.props.inputText !== prevProps.inputText) {
      this.inputRef.focus();
      if (this.inputCursorPosition) {
        this.inputRef.setSelectionRange(
          this.inputCursorPosition,
          this.inputCursorPosition
        );
      }
    }
  }

  render() {
    const {
      tags,
      isMultiValue,
      placeholderText,
      inputText
    } = this.props;

    const searchInput = tags; //Tags + search value

    return (
      <SearchField>
        <CreatableSelect
          ref={elem => {
            this.selectRef = elem;
          }}
          isMulti={isMultiValue}
          autoBlur
          isSearchable
          isClearable={false}
          components={{
            Menu: props => null,
            DropdownIndicator,
            MultiValueContainer: props => null,
            ValueContainer: props => {
              const { children, ...otherProps } = props;
              return (
                <Fragment key="cts-input-fragment">
                  <components.ValueContainer
                    {...otherProps}
                    key="cts-input-container"
                  >
                    <SearchFieldInput
                      innerRef={ref => {
                        this.inputRef = ref;
                      }}
                      key="cts-input-text-field"
                      placeholder={placeholderText}
                      value={inputText}
                      onChange={this.handleInputChangeEvent}
                      onKeyDown={this.onKeyDown}
                    />
                    <TagWrapper>
                      {tags.map((tag, i) => (
                        <Tooltip content={tag.label} key={`filter-${i}`}>
                          <Tag
                            elemBefore={
                              <Avatar
                                size="xsmall"
                                src={
                                  tag.iconUrl ||
                                  `${
                                    this.props.baseUrl
                                  }/images/logo/default-space-logo-256.png`
                                }
                              />
                            }
                            text={tags.length > 1 ? null : tag.label}
                            removeButtonText="remove"
                            onBeforeRemoveAction={() => {
                              this.onRemoveTag(tag);
                            }}
                          />
                        </Tooltip>
                      ))}
                    </TagWrapper>
                  </components.ValueContainer>
                </Fragment>
              );
            }
          }}
          options={tags}
          placeholder={placeholderText}
          value={searchInput}
          styles={getCustomStyles}
        />
      </SearchField>
    );
  }
}
