// @flow

import styled from "styled-components";

import { gridSize, textColor, borderRadius } from "../../../common/styles";
import { colors } from "@atlaskit/theme";

const SearchFieldBgColor = colors.N30;

export const SearchFieldInput = styled.input`
  border: 0;
  background: none;
  font-size: 14px;
  color: ${textColor};
  min-width: 0px;
  text-overflow: ellipsis;
  border-radius: ${borderRadius}px;
  &:hover {
    outline: 0;
  }
  &:focus {
    outline: 0;
  }
  width: 100%;
`;

export const SearchField = styled.div`
  background: ${SearchFieldBgColor};
  flex: 1;
  &:focus {
    outline: 0;
    border: 1px solid #4c9aff;
  }
`;

export const TagWrapper = styled.div`
  display: flex;
`;

export const SearchIconWrapper = styled.div`
  color: ${colors.N70};
  margin-right: ${gridSize / 2}px;
`;
