// @flow

import React, {Fragment, PureComponent} from "react";
import {injectIntl, type IntlShape} from "react-intl";
import {FormWrapper} from "./styles";
import SearchInput from "./input/index";
import {AutoDismissFlag, FlagGroup} from "@atlaskit/flag";
import {amountOptions, orderOptions, type Option} from "../../common/constants";
import {ERROR, LOADING, SEARCH_MODE_CQL, SEARCH_MODE_NORMAL, SUCCESS, type LoadingStatus, NOT_STARTED} from "../common/constants";
import {addPageTypeToCQL, buildCqlByFilter, generateCqlBySearchParams, getOrderByCql, transformCqlToSearchParams} from "../../common/utils";
import History from "./history";
import messages from "./messages";
import ResultsPanel from "./results-panel"
import {startTreeLoadProgressivelyByCQL} from "../../../search/TreeSearcher";
import type {DTreeSearchResult} from "../../../search/model/DTreeSearchResult";
import {getConfluenceBaseUrl} from "../../../utils/APHelper";
import SearchControls from "./controls";
import {colors} from "@atlaskit/theme";
import Error from "@atlaskit/icon/glyph/error";

type FlagItem = {
  description: string,
  icon: any,
  title: string
};

const history = new History();
const initialQuery = history.getQuery();

type Props = {
  intl: IntlShape
};

type State = {
  searchFilters: Option[], //Search filters like author or parent page
  searchInputText: string, //Textual input of the search (CQL query or search text)
  orderBy: string,
  maxResults: number,
  isDefaultExpanded: boolean,
  searchMode: typeof SEARCH_MODE_CQL | typeof SEARCH_MODE_NORMAL,
  loadingStatus: LoadingStatus,
  errorText: string | void,
  searchResult: DTreeSearchResult | void,
  progress: number,
  baseUrl: string,
  cqlQuery: string,
  flags: FlagItem[]
};
export const FilterContext = React.createContext({
  searchFilters: [],
  searchInputText: ""
});

export class SearchForm extends PureComponent<Props, State> {
  state: State = {
    cqlQuery: initialQuery.cqlQuery || "", //TODO Remove cqlQuery from state, because it duplicates searchInputText (cqlQuery = searchInputText when mode = CQL)
    searchFilters: initialQuery.filters,
    searchInputText: initialQuery.text || "",
    orderBy: orderOptions[0].value,
    maxResults: amountOptions[0].value,
    loadingStatus: NOT_STARTED,
    isDefaultExpanded: true,
    searchMode: initialQuery.mode,
    baseUrl: "",
    progress: 0,
    errorText: undefined,
    searchResult: undefined,
    flags: []
  };

  componentDidMount() {

    getConfluenceBaseUrl(baseUrl => this.setState({baseUrl: baseUrl}));

    history.addListener(event => {
      if (this.state.loadingStatus === LOADING) {
        return;
      }

      const query = history.getQuery();
      this.setState({
        cqlQuery: query.cqlQuery || "",
        searchFilters: query.filters || [],
        searchInputText: query.text || "",
        searchMode: query.mode || SEARCH_MODE_NORMAL
      });
    });

    this.search({source: "page"});
  }

  dismissFlag = () => {
    this.setState(state => ({flags: []}));
  };

  onOrderByChange = selected => {
    this.setState({
      orderBy: selected.value
    });
  };

  onAmountChange = selected => {
    this.setState({
      maxResults: selected.value
    });
  };

  onToggleSearchMode = () => {
    let searchMode: string = SEARCH_MODE_NORMAL;

    if (this.state.searchMode === SEARCH_MODE_NORMAL) {
      searchMode = SEARCH_MODE_CQL;
      const query = generateCqlBySearchParams(
        this.state.searchInputText,
        this.state.searchFilters
      );

      return this.setState({cqlQuery: query, searchMode});
    } else {
      const newSearchText = transformCqlToSearchParams(this.state.cqlQuery)
        .text;

      this.setState({searchInputText: newSearchText, searchMode});
    }
  };

  onAddFilter = (newFilter: Option) => {
    if (this.state.searchMode === SEARCH_MODE_NORMAL) {
      //Check if filter already exists and return if it is
      if (this.state.searchFilters) {
        const found = this.state.searchFilters.filter(
          input => input.value === newFilter.value
        );
        if (found.length > 0) {
          return false;
        }
      }

      this.updateFiltersAndPerformSearch([
        ...this.state.searchFilters,
        {...newFilter}
      ]);
    } else {
      const filterCql = buildCqlByFilter(newFilter);
      if (this.state.cqlQuery.indexOf(filterCql) === -1) {
        const newCql = this.state.cqlQuery + " AND " + filterCql;
        this.setState({cqlQuery: newCql});
        this.search({text: newCql, source: "filtersUpdated_Cql"});
      }
    }
  };

  onFiltersChanged = (filters: Option[]) => {
    this.updateFiltersAndPerformSearch(filters);
  };

  updateFiltersAndPerformSearch = (newFilters: Option[]) => {

    this.search({filters: newFilters, source: "filtersUpdated"});
  };

  /**
   * We put text and filters in this method parameters, because it might be called before state is updated.
   *
   * When we call setState, state will not be updated instatnly. State will be updated asynchronously.
   * That's why we need to override search params, when we call search() method from code which change state of searchInputText or filters
   *
   * @param text
   * @param filters
   * @param source
   * @returns {boolean}
   */
  search = ({text, filters, source}) => {
    source = source || "unknown";

    const {
      searchMode,
      searchInputText,
      cqlQuery,
      searchFilters,
      orderBy
    } = this.state;

    let cql = text || cqlQuery;
    text = text || searchInputText;
    filters = filters || searchFilters;

    //Update search history
    if (searchMode === SEARCH_MODE_NORMAL) {
      history.pushQuery({
        text: text,
        filters: this.state.searchFilters,
        mode: searchMode
      });
    } else {
      history.pushQuery({
        cqlQuery: cql,
        filters: this.state.searchFilters,
        mode: searchMode
      });
    }

    if (searchMode === SEARCH_MODE_NORMAL) {
      cql = this.createCqlFromFormParams(text, filters, orderBy);
    } else {
      cql = addPageTypeToCQL(cql); //We need to enforce PAGE type even when we use custom CQL. Confluence Tree Search can't handle other data types yet.
    }

    if (!cql || cql === "") {
      return false;
    }
    this.setState({
      loadingStatus: LOADING,
      progress: 0,
      searchResult: undefined,
      searchFilters: filters
    });

    const maxResults = this.state.maxResults;

    startTreeLoadProgressivelyByCQL(
      cql,
      maxResults,
      this.updateSearchTree,
      this.searchComplete,
      this.searchErrorHandler
    );

  };

  createCqlFromFormParams = (searchText, searchFilters, orderBy) => {
    //Create base part of CQL from text input and additional criterias like contributors and ancestors
    let cql: string = generateCqlBySearchParams(searchText, searchFilters);

    //Restrict all results to have CQL type
    cql = addPageTypeToCQL(cql);

    //Set ordering
    return cql + getOrderByCql(orderBy);
  };

  getProgressInPercent = (resultsLoadedSoFar: number): number => {
    return 100 * (resultsLoadedSoFar / this.state.maxResults);
  };

  updateSearchTree = (tree: DTreeSearchResult) => {
    const progressInPercent = this.getProgressInPercent(tree.totalResults);
    this.setState({
      searchResult: tree,
      loadingStatus: LOADING,
      progress: progressInPercent
    });
  };

  searchErrorHandler = error => {
    const {
      intl: {formatMessage}
    } = this.props;
    this.setState({
      loadingStatus: ERROR,
      errorText: error,
      flags: [
        {
          title: formatMessage(messages.cqlErrorTitle),
          description: formatMessage(messages.cqlErrorDescription),
          icon: <Error label="Error icon" primaryColor={colors.R300}/>
        }
      ]
    });
  };

  searchComplete = () => {
    this.setState({
      loadingStatus: SUCCESS,
      progress: 100,
      flags: []
    });
  };

  onEnterPressed = () => {
    this.search({source: "enterPressed"});
  };

  onInputOnChange = value => {
    const {searchMode} = this.state;

    if (searchMode === SEARCH_MODE_NORMAL) {
      this.setState({
        searchInputText: value
      });
    } else {
      this.setState({
        cqlQuery: value
      });
    }
  };

  render() {
    const {
      intl: {formatMessage}
    } = this.props;
    const {
      searchInputText,
      cqlQuery,
      searchFilters,
      searchMode,
      orderBy,
      loadingStatus,
      maxResults,
      searchResult,
      errorText,
      baseUrl,
      isDefaultExpanded
    } = this.state;


    const placeholderText =
      searchMode === SEARCH_MODE_NORMAL
        ? formatMessage(messages.placeholderNormal)
        : formatMessage(messages.placeholderCql);

    const filters = searchMode === SEARCH_MODE_NORMAL ? searchFilters : [];
    const text = searchMode === SEARCH_MODE_NORMAL ? searchInputText : cqlQuery;

    const isMultiValue = searchMode === SEARCH_MODE_NORMAL;

    const filter = {
      searchFilters: filters,
      searchText: text
    };
    return (
      <FilterContext.Provider value={filter}>
        <FlagGroup onDismissed={this.dismissFlag}>
          {this.state.flags.map(flag => (
            <AutoDismissFlag {...flag} />
          ))}
        </FlagGroup>
        <FormWrapper>
          <SearchInput
            tags={filters}
            isMultiValue={isMultiValue}
            placeholderText={placeholderText}
            inputText={text}
            onInputChange={this.onInputOnChange}
            onEnterPressed={this.onEnterPressed}
            baseUrl={this.state.baseUrl}
            onTagsChange={this.onFiltersChanged}
          />

          <SearchControls
            searchMode={searchMode}
            onToggleSearchMode={this.onToggleSearchMode}
            onSearch={() => {
              this.search({source: "button"});
            }}
            orderBy={orderBy}
            maxResults={maxResults}
            onOrderByChange={this.onOrderByChange}
            onAmountChange={this.onAmountChange}
          />
        </FormWrapper>

        <ResultsPanel searchResult={searchResult} loadingStatus={loadingStatus} errorText={errorText}
          orderBy={orderBy} baseUrl={baseUrl} isDefaultExpanded={isDefaultExpanded} onAddFilter={this.onAddFilter}/>

      </FilterContext.Provider>
    );
  }
}

export default injectIntl(SearchForm);
