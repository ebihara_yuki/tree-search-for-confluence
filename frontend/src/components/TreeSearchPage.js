// @flow
import React, { PureComponent } from "react";

import Page from "@atlaskit/page";
import styled from "styled-components";

import Header from "./header";
import { SearchForm } from "./search";
import { IntlProvider } from "react-intl";
import { gridSize } from "./common/styles";

const PageWrapper = styled.div`
  margin: 0 ${gridSize * 5}px;
`;

export default class TreeSearchPage extends PureComponent<{}> {

  render() {
    return (
      <IntlProvider locale="en">
        <Page>
          <PageWrapper className="_ts_main_page_wrapper">
            <Header />
            <SearchForm />
          </PageWrapper>
        </Page>
      </IntlProvider>
    );
  }
}
