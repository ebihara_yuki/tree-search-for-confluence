// @flow

import React from 'react'
import { Line } from 'rc-progress'

type Props = {
  progress: number
}

export default (props: Props) => <Line
  percent={props.progress}
  strokeColor='#FC9F00'
  style={{
    transition: 'stroke-dashoffset 3s ease 0s, stroke 3s linear !important',
  }}
/>
