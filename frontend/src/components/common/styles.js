// @flow

import {
  gridSize as akGridSize,
  fontSize as akFontSize,
  fontFamily as akFontFamily
} from "@atlaskit/theme";
import { colors } from "@atlaskit/theme";

export const gridSize = akGridSize();
export const fontSize = akFontSize();
export const fontFamily = akFontFamily();
export const textColor = colors.N800;
export const borderRadius = 3;
