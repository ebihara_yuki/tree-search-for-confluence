// @flow

export const SPACE_SEARCH = "SPACE_SEARCH";
export const CONTRIBUTOR_SEARCH = "CONTRIBUTOR_SEARCH";
export const PAGE_SEARCH = "PAGE_SEARCH";
export const TEXT_SEARCH = "TEXT_SEARCH";

export type Option = {|
  label: string,
  value: string | number,
  iconUrl?: string,
  type?: string
|};

export const UPDATED_RECENTLY = "updated";
export const CREATED_RECENTLY = "created";
export const AMOUNT_OF_RESULT = "count";

export const orderOptions: Option[] = [
  { label: "Relevance", value: "none" },
  { label: "Updated recently", value: UPDATED_RECENTLY },
  { label: "Created recently", value: CREATED_RECENTLY },
  { label: "Amount of results", value: AMOUNT_OF_RESULT }
];
export const amountOptions: Option[] = [
  { label: "30", value: 30 },
  { label: "60", value: 60 },
  { label: "100", value: 100 }
];
