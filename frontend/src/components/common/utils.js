// @flow
import {
  CONTRIBUTOR_SEARCH,
  PAGE_SEARCH,
  SPACE_SEARCH,
  CREATED_RECENTLY,
  UPDATED_RECENTLY
} from "./constants";

export const KEY_ENTER = 13;

export const parseQueryString = () => {
  var str = window.location.search;
  var objURL = {};

  str.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function(
    $0,
    $1,
    $2,
    $3
  ) {
    objURL[$1] = $3;
  });

  return objURL;
};

export const getUrlQParam = pName => {
  var res = parseQueryString()[pName];
  return res;
};

// TODO shipit last minute fix. Replace with a lib function
export const queryUrlParamToSearchInput = () => {
  var str = getUrlQParam("query");
  if (str) {
    return decodeURI(str.replace("+", " "));
    /* decodeURI() is for non-english andspecial symbols */
  }
};

// making flow happy
export const getFormVal = (form: HTMLFormElement, name: string): string => {
  // el is actually a type of HTMLElement but flow is unhappy when I do that
  const el: Object = form.elements.namedItem(name);
  if (typeof el.value === "string") {
    return el.value;
  }
  return "";
};

export const getSearchParamByQueryArray = queryArray => {
  //TODO
  //cql = textInput === "" ? 'siteSearch ~ "" and type = "page"' : textInput;
};

const buildORCqlByFilterArray = (filterArray: any): string => {
  const CQL: string =
    filterArray.length > 0
      ? " AND (" +
        filterArray.map(query => buildCqlByFilter(query)).join(" OR ") +
        ")"
      : "";
  return CQL;
};

export const buildCqlByFilter = query => {
  return `${cqlKeyMapping[query.type]}="${query.value}"`;
};

export const getOrderByCql = (orderBy: string) => {
  switch (orderBy) {
  case UPDATED_RECENTLY:
    return " order by lastModified desc";
  case CREATED_RECENTLY:
    return " order by created desc";
  default:
    return "";
  }
};

const cqlKeyMapping = {
  SPACE_SEARCH: "space",
  CONTRIBUTOR_SEARCH: "contributor.accountId",
  PAGE_SEARCH: "ancestor"
};

export const getSearchFilterCQL = queryArray => {
  let parentCQL: string = "";
  let contributorCQL: string = "";
  let spaceCQL: string = "";
  if (queryArray) {
    const spaceSearch = queryArray.filter(
      search => search.type === SPACE_SEARCH
    );
    const contributorSearch = queryArray.filter(
      search => search.type === CONTRIBUTOR_SEARCH
    );
    const parentSearch = queryArray.filter(
      search => search.type === PAGE_SEARCH
    );
    spaceCQL = buildORCqlByFilterArray(spaceSearch);
    contributorCQL = buildORCqlByFilterArray(contributorSearch);
    parentCQL = buildORCqlByFilterArray(parentSearch);
  }

  return contributorCQL + parentCQL + spaceCQL;
};

export const generateCqlBySearchParams = (textInput, filters) => {
  const inputValue = textInput.replace(/\"/g, '\\"');
  const filterCql = getSearchFilterCQL(filters);
  const cql: string = `siteSearch ~ "${inputValue}" ${filterCql}`;

  return cql;
};

export const addPageTypeToCQL = cql => {
  return cql + ' and type = "page"';
};

export type SearchParams = {
  text: string,
  ancestors: string[],
  contributors: string[]
};

export function transformCqlToSearchParams(cql: string): SearchParams {
  const siteSearch = /siteSearch\s?~\s?"([^"]+)"/.exec(cql);
  const siteSearchValue =
    siteSearch && siteSearch.length > 1 ? siteSearch[1] : null;
  const text = siteSearchValue || "";
  //TODO Add transformation of ancestors and contributors
  return { text, ancestors: [], contributors: [] };
}
