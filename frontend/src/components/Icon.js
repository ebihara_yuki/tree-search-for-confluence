// @flow

import React from 'react';

type Props = {
    icon: Icon | null,
    baseUrl: string,
}

export type Icon = {
    height: number,
    width: number,
    isDefault: boolean,
    path: string
};

function scaleIconSize(sz) {
  return Math.round(sz * 0.875); // to make icons smaller, to convert 48px to 40px e.g.
}

export default ({icon, baseUrl}: Props) => {
  if (!icon) {
    return null
  }
  const imageUrl = baseUrl + icon.path


  return (
    <img
      className="_ts_path_picture"
      src={imageUrl}
      height={scaleIconSize(icon.height)}
      width={scaleIconSize(icon.width)}
    />
  )
}
