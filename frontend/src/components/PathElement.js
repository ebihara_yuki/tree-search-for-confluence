// @flow

import React, {PureComponent} from "react";
import "../css/TreeSearch.css";
import Icon from "./Icon";
import type {DPathElement} from "../search/model/DPathElement";
import {openConfluencePage} from "../utils/APHelper";

type Props = {
  pathElement: DPathElement,
  baseUrl: string,
}

export default class PathElement extends PureComponent<Props> {
  //Data object example
  //  "pathElement": {
  //      "id": 2221323862,
  //      "title": "Abandoned Plans of Bamboo Servers",
  //      "link": "/display/RELENG/Abandoned+Plans+of+Bamboo+Servers"
  //  }

  render() {
    let pathElement = this.props.pathElement;
    const url = this.props.baseUrl + pathElement.link;

    const icon = pathElement.icon;

    return (
      <span className="_ts_header_path_item">
        <Icon baseUrl={this.props.baseUrl} icon={icon}/>
        <a
          href={url}
          target="_parent"
        >{pathElement.title}</a>
      </span>
    );
  }
}
