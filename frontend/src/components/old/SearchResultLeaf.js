// @flow

import React, { PureComponent } from "react";
import "../../css/legacy/confluence-search.css";
import "../../css/TreeSearch.css";
import PageIcon from "@atlaskit/icon/glyph/media-services/document";
import {COUNTER_SIZE_PX, type LeafResult} from "./SearchResultNode";
import ReactHTMLConverter from "react-html-converter";
import { getOpacityByDate } from "../../utils/pageAgeing";
import type {DSingleSearchResult} from "../../search/model/DSingleSearchResult";
import {openConfluencePage} from "../../utils/APHelper";

type Props = {
  data: DSingleSearchResult,
  baseUrl: string,
  pageAgeing: boolean,
}

export default class SearchResultLeaf extends PureComponent<Props> {
  converter = new ReactHTMLConverter();

  // Data example:
  //
  // "result": {
  //     "id": 2961263257,
  //     "type": {
  //         "value": "page",
  //         "type": "page",
  //         "builtIn": true
  //     },
  //     "title": "@@@hl@@@bitbucket@@@endhl@@@-bamboo abandoned builds",
  //     "excerpt": "run in 299 days. plantemplates https://bitbucketbamboo.internal.atlassian.com/browse/label/plantemplates @@@hl@@@Bitbucket@@@endhl@@@
  // Cloud Libraries Django Media @@@hl@@@API@@@endhl@@@ Client &hellip; Abandoned plans Enabled Status Details Tags @@@hl@@@Bitbucket@@@endhl@@@
  // Billing Service @@@hl@@@Bitbucket@@@endhl@@@ Python App Base Image",
  //     "links": {
  //         "webui": {
  //             "type": {
  //                 "value": "webui",
  //                 "type": "webui"
  //             },
  //             "path": "/display/RELENG/bitbucket-bamboo+abandoned+builds"
  //         },
  //         "tinyui": {
  //             "type": {
  //                 "value": "tinyui",
  //                 "type": "tinyui"
  //             },
  //             "path": "/x/mUqBs"
  //         }
  //     },
  //     "friendlyLastModified": "about 5 hours ago"
  // },

  render() {
    return this.buildResultsPage();
  }

  highlight(text: string) {
    var upd = text.replace(/@@@hl@@@/g, "<strong>");
    upd = upd.replace(/@@@endhl@@@/g, "</strong>");
    return this.converter.convert(upd);
  };

  buildResultsPage() {
    const data = this.props.data;
    const link = this.props.baseUrl + this.props.data.links.webui;
    const pageAgeingOpacity = this.props.pageAgeing
      ? getOpacityByDate(data.timestamp)
      : 1;

    return (
      <article
        className="_ts_results_list_item"
        style={{
          margin: "10px 0 14px " + COUNTER_SIZE_PX + "px",
          opacity: pageAgeingOpacity
        }}
      >
        <h3>
          <span style={{ color: "blue", fill: "white" }}>
            <PageIcon secondaryColor="inherit" label="Page" />
          </span>
          <a
            className="search-result-link visitable"
            target="_parent"
            href={link}
            data-type={data.type}
          >
            {this.highlight(data.title)}
          </a>
        </h3>
        <div className="highlights">{this.highlight(data.excerpt)}</div>
        <div className="search-result-meta">
          {/*<a class="container" href="{contextPath() + $resultGlobalContainer.displayUrl}" title="{getText('confluence-search.results.location')}">Container_Placeholder</a>

                <span> / &#8230; / </span>
                <a class="container" href="{contextPath() + $resultParentContainer.displayUrl}" title="{getText('confluence-search.results.page.location')}">{$resultParentContainer.title}</a>
                <span> / &#8230; / </span>*/}

          <span className="date" title="last modified date">
            {data.friendlyLastModified}
          </span>
        </div>
      </article>
    );
  }
}
