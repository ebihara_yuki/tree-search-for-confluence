// @flow

import React, { Component } from "react";
import Button, { ButtonGroup } from "@atlaskit/button";
import Spinner from "@atlaskit/spinner";
import ProgressBar from "./ProgressBar";
import { FieldTextStateless } from "@atlaskit/field-text";
import Checkbox from "@atlaskit/checkbox";
import styled from "styled-components";
import Select from "@atlaskit/select";
import SearchResultsOld from "./SearchResultsOld";
import "../css/TreeSearch.css";
import { publishQueryEvent } from "../statisticsCollector";
import {
  getPageAgeingSetting,
  savePageAgeingSetting
} from "../utils/pageAgeing";
import {
  startTreeLoadProgressively,
  startTreeLoadProgressivelyByCQL,
  transformCqlToSearchParams,
  transformSearchParamsToCQL
} from "../search/TreeSearcher";
import type { DTreeSearchResult } from "../search/model/DTreeSearchResult";
import { getConfluenceBaseUrl } from "../utils/APHelper";

const SearchRow = styled.div`
  display: flex;
  flex-direction: row;
  width: 65%;
`;

const SearchText = styled.div`
  flex-grow: 2;
`;

const SelectBox = styled.div`
  width: 125px;
  padding-left  15px;
`;

const StyledButton = styled(Button)`
  margin: 3px 10px;
`;

type LoadingStatus = "NOT_STARTED" | "LOADING" | "SUCCESS" | "ERROR";
const NOT_STARTED = "NOT_STARTED";
const LOADING = "LOADING";
const SUCCESS = "SUCCESS";
const ERROR = "ERROR";

const SEARCH_MODE_CQL = "CQL";
const SEARCH_MODE_NORMAL = "NORMAL";

const maxResultsOptions = [
  {
    heading: "Max Results",
    items: [
      { content: "30", value: 30 },
      { content: "60", value: 60 },
      { content: "120", value: 120 },
      { content: "200", value: 200 },
      { content: "300", value: 300 },
      { content: "500", value: 500 },
      { content: "1000", value: 1000 }
    ]
  }
];

const defaultMaxResults = maxResultsOptions[0].items[0];

export type ProgressCallback = DTreeSearchResult => void;
export type CompleteCallback = void => void;

// making flow happy
function val(form: HTMLFormElement, name: string): string {
  // el is actually a type of HTMLElement but flow is unhappy when I do that
  const el: Object = form.elements.namedItem(name);
  if (typeof el.value === "string") {
    return el.value;
  }
  return "";
}

type Props = {};

type State = {
  searchInput: string,
  pageAgeing: boolean,
  loadingStatus: LoadingStatus,
  searchResult: DTreeSearchResult | void,
  searchMode: "CQL" | "NORMAL",
  maxResults: number,
  progress: number
};

// TODO this is a fast solution from SO for ShipIt last minute change. Replace with a f from a lib
var parseQueryString = function() {
  var str = window.location.search;
  var objURL = {};

  str.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function(
    $0,
    $1,
    $2,
    $3
  ) {
    objURL[$1] = $3;
  });

  return objURL;
};

function getUrlQParam(pName) {
  var res = parseQueryString()[pName];
  return res;
}

// TODO shipit last minute fix. Replace with a lib function
function queryUrlParamToSearchInput() {
  var str = getUrlQParam("query");
  if (str) {
    return decodeURI(str.replace("+", " "));
    /* decodeURI() is for non-english andspecial symbols */
  }
}

export default class SearchForm extends Component<Props, State> {
  state = {
    loadingStatus: NOT_STARTED,
    searchInput: queryUrlParamToSearchInput(),
    searchResult: undefined,
    searchMode: SEARCH_MODE_NORMAL,
    pageAgeing: getPageAgeingSetting(),
    maxResults: defaultMaxResults.value,
    baseUrl: "",
    progress: 100
  };

  componentDidMount() {
    var query = this.state.searchInput;
    if (query) {
      this.filterChange(query, 30); // TODO read 30 from defaults settings
    }

    getConfluenceBaseUrl(baseUrl => this.setState({ baseUrl: baseUrl }));
  }

  onFormSubmit = (e: SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault();
    const form: HTMLFormElement = e.currentTarget;
    const query = val(form, "query");
    const maxResults = Number.parseInt(val(form, "maxResults"), 10);
    this.setState({ maxResults });
    if (query && !isNaN(maxResults) && maxResults > 0) {
      this.filterChange(query, maxResults);
    }
  };

  filterChange = (inputValue: string, maxResults: number) => {
    this.setState({
      loadingStatus: LOADING,
      progress: 0,
      searchResult: undefined
    });

    let cql;
    if (this.state.searchMode === SEARCH_MODE_CQL) {
      startTreeLoadProgressivelyByCQL(
        inputValue,
        maxResults,
        this.updateSearchTree,
        this.searchComplete
      );
    } else {
      startTreeLoadProgressively(
        { text: inputValue },
        maxResults,
        this.updateSearchTree,
        this.searchComplete
      );
    }

    publishQueryEvent();
  };

  getProgressInPercent(resultsLoadedSoFar: number): number {
    return 100 * (resultsLoadedSoFar / this.state.maxResults);
  }

  updateSearchTree = (tree: DTreeSearchResult) => {
    this.setState({
      searchResult: tree,
      loadingStatus: LOADING,
      progress: this.getProgressInPercent(tree.totalResults)
    });
  };

  searchComplete = () => {
    this.setState({
      loadingStatus: SUCCESS,
      progress: 100
    });
  };

  searchResults = () => {
    const { searchResult, loadingStatus, pageAgeing } = this.state;

    if (loadingStatus === LOADING && !searchResult) {
      // intentionally start on 1% so that the progress bar line appears
      return (
        <div>
          <ProgressBar progress={1} />
          <Spinner />
        </div>
      );
    }

    if (loadingStatus === NOT_STARTED) return null;

    if (loadingStatus === ERROR) {
      return <p>Error loading search results</p>;
    }

    if (searchResult) {
      return (
        <section>
          {this.state.loadingStatus === LOADING && (
            <ProgressBar progress={this.state.progress} />
          )}
          <SearchResultsOld
            tree={searchResult.tree}
            pageAgeing={pageAgeing}
            confluenceUrl={this.state.baseUrl}
          />
        </section>
      );
    }
  };

  changeSearchMode(mode: "CQL" | "NORMAL") {
    if (this.state.searchMode !== mode) {
      const currentQuery = this.state.searchInput;
      const newQuery =
        mode === "CQL"
          ? transformSearchParamsToCQL({ text: currentQuery })
          : transformCqlToSearchParams(currentQuery).text;

      this.setState({
        searchMode: mode,
        searchInput: newQuery
      });
    }
  }

  changePageAgeing(newValue: boolean) {
    this.setState({
      pageAgeing: newValue
    });
    savePageAgeingSetting(newValue);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onFormSubmit}>
          {this.buildSearchModeChooseRow()}
          {this.buildSearchInputBoxRow()}
          {this.buildPageAgeingRow()}
        </form>
        {this.searchResults()}
      </div>
    );
  }

  buildPageAgeingRow() {
    const checkedParam = this.state.pageAgeing
      ? { initiallyChecked: true }
      : {};

    return (
      <section>
        <Checkbox
          {...checkedParam}
          label="Page ageing"
          value="Page ageing"
          onChange={({ isChecked }) => this.changePageAgeing(isChecked)}
          name="page-ageing"
        />
      </section>
    );
  }

  updateSearchInput = (e: SyntheticEvent<HTMLInputElement>) => {
    this.setState({
      searchInput: e.currentTarget.value
    });
  };

  buildSearchInputBoxRow() {
    return (
      <SearchRow>
        <SearchText>
          <FieldTextStateless
            autoFocus
            type="text"
            name="query"
            value={this.state.searchInput}
            onChange={this.updateSearchInput}
            placeholder="Search..."
            isLabelHidden
            shouldFitContainer
          />
        </SearchText>
        <SelectBox>
          <Select
            options={maxResultsOptions}
            name="maxResults"
            isSearchable
            defaultValue={defaultMaxResults}
          />
        </SelectBox>
        <StyledButton appearance="primary" type="submit">
          Go!
        </StyledButton>
      </SearchRow>
    );
  }

  buildSearchModeChooseRow() {
    return (
      <section>
        <span className="_ts_search_mode_label">Search mode:</span>
        <span>
          <ButtonGroup>
            <Button
              appearance={
                this.state.searchMode === SEARCH_MODE_NORMAL
                  ? "primary"
                  : "subtle"
              }
              onClick={() => {
                this.changeSearchMode(SEARCH_MODE_NORMAL);
              }}
            >
              Normal
            </Button>
            <Button
              appearance={
                this.state.searchMode === SEARCH_MODE_CQL ? "primary" : "subtle"
              }
              onClick={() => {
                this.changeSearchMode(SEARCH_MODE_CQL);
              }}
            >
              CQL
            </Button>
          </ButtonGroup>
        </span>
      </section>
    );
  }
}
