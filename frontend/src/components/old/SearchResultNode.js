// @flow

import React, {Component, type Element} from "react";
import PathElement from "../PathElement";
import {buildNestedResultsElement, EXPAND_STATE_COLLAPSED, EXPAND_STATE_EXPANDED, EXPAND_STATE} from "./SearchResultsOld";
import SearchResultLeaf from "./SearchResultLeaf";
import "../../css/TreeSearch.css";
import {getOpacityByDate} from "../../utils/pageAgeing";
import type {DSearchResultTreeNode} from "../../search/model/DSearchResultTreeNode";
import type {DPathElement} from "../../search/model/DPathElement";
import type {ContentId} from "../../search/model/DContentId";

export const COLEXP_BTN_SIZE_PX = 20;
export const COUNTER_SIZE_PX = 33; // TODO improve, do not hardcode?

export type PathElementId = ContentId;


type Props = {
  pageAgeing: boolean,
  baseUrl: string,
  data: Tree,
  isExpanded: PathElementId => boolean,
  setExpanded: (id: PathElementId, expanded: EXPAND_STATE) => void,
};

type State = {
  expanded: boolean
};

type HeaderPath = {
  breadcrumbs: DPathElement[],
  leaf: DSearchResultTreeNode
};

/**
 * When expanding a node, we do want to display all paths with only 1 child as part of single header.
 * IntelliJ and GitHub and Bitbucket do the same: if a folder has only 1 subfolder, step into it right away.
 */
function calculatePath(tree: DSearchResultTreeNode): HeaderPath {
  let current = tree;
  // start with first pathElement because we always want to render that
  const breadcrumbs = [current.pathElement];

  while (current) {
    const childrenMap = current.children;
    if (!childrenMap) {
      break
    }

    const keys = Object.keys(childrenMap);
    // iff:
    // there is only 1 child subfolder, and
    // that subfolder has _nothing_ to display (no result)
    // then append it as part of path in header, and iterate deeper
    if (keys.length === 1 && !current.result) {
      const child = childrenMap[keys[0]];
      breadcrumbs.push(child.pathElement);
      current = child;
    } else {
      // we have arrived to the end of path; whatever is left will be rendered as a proper result
      break
    }
  }

  return {
    breadcrumbs,
    leaf: current
  }

}

export default class SearchResultNode extends Component<Props, State> {
  state = {
    expanded: this.props.isExpanded(this.props.data.pathElement.id)
  };

  static getDerivedStateFromProps(nextProps: Props) {
    return {
      expanded: nextProps.isExpanded(nextProps.data.pathElement.id)
    };
  }

  expand = () => {
    this.setState({
      expanded: true
    });
    this.props.setExpanded(this.props.data.pathElement.id, EXPAND_STATE_EXPANDED);
  };

  collapse = () => {
    this.setState({
      expanded: false
    });
    this.props.setExpanded(this.props.data.pathElement.id, EXPAND_STATE_COLLAPSED);
  };

  buildDisplayablePath(breadcrumbs: DPathElement[]): Element<typeof PathElement>[] {
    return breadcrumbs.map(this.buildPathElement);
  }

  render() {
    const data = this.props.data;
    const count = data.count;

    const expanded = this.state.expanded;

    const pageAgeingStyle = this.props.pageAgeing
      ? {opacity: getOpacityByDate(data.lastModificationTimestamp)}
      : {};

    const resultsCounter = SearchResultNode.buildResultsCounter(expanded, count);

    const colexpBlock = this.buildColexpButton(expanded);

    const lastModifiedBlock = this.props.pageAgeing
      ? SearchResultNode.buildLatestModifiedDate(data.lastModificationTimestamp)
      : "";

    if (expanded) {
      const {breadcrumbs, leaf} = calculatePath(this.props.data);
      return (
        <div>
          <p className="_ts_header_path" style={pageAgeingStyle}>
            {colexpBlock}
            {resultsCounter}
            {this.buildDisplayablePath(breadcrumbs)}
          </p>
          <div style={{margin: "5px 0 5px " + COLEXP_BTN_SIZE_PX + "px"}}>
            {this.buildResultDiv(leaf)}
            {buildNestedResultsElement(
              leaf.children,
              this.props.baseUrl,
              this.props.pageAgeing,
              this.props.isExpanded,
              this.props.setExpanded
            )}
          </div>
        </div>
      );
    } else {
      const pathElement = this.buildPathElement(data.pathElement);
      return (
        <div>
          <p>
            {colexpBlock}
            <span style={pageAgeingStyle}>
              {resultsCounter}
              {pathElement}
              {lastModifiedBlock}
            </span>
          </p>
        </div>
      );
    }
  }

  static buildLatestModifiedDate(dateMillis: number) {
    const date = new Date(dateMillis);
    return (
      <span className="_ts_last_modified_space">
        (Modified: {date.getDate()}-{date.getMonth() + 1}-{date.getFullYear()})
      </span>
    );
  }

  buildPathElement = (pe: DPathElement): Element<typeof PathElement> =>
    <PathElement key={pe.id} pathElement={pe} baseUrl={this.props.baseUrl}/>

  static buildResultsCounter(expanded: boolean, count: number) {
    const resultsCounter = (
      <span
        style={{
          width: COUNTER_SIZE_PX + "px",
          display: "inline-block",
          visibility: expanded ? "hidden" : "visible",
          textAlign: "center",
          padding: "0 4px"
        }}
      >
        <b style={{margin: "0px 3px 0px 3px"}}>{count}</b>
      </span>
    );
    return resultsCounter;
  }

  buildColexpButton(expanded: boolean) {
    const colexpBtn = expanded ? (
      <a onClick={this.collapse} style={{fontSize: '14px'}} className="_ts_link_button">
        (-)
      </a>
    ) : (
      <a onClick={this.expand} style={{fontSize: '14px'}} className="_ts_link_button">
        (+)
      </a>
    );

    return (
      <span
        style={{
          display: 'inline-block',
          width: COLEXP_BTN_SIZE_PX + "px",
          textAlign: 'center'
        }}
      >
        {colexpBtn}
      </span>
    );
  }

  buildResultDiv(leaf: Tree) {
    const result = leaf.result;
    if (!result) {
      return null
    }
    return (
      <SearchResultLeaf
        data={result}
        baseUrl={this.props.baseUrl}
        pageAgeing={this.props.pageAgeing}
      />
    );
  }
}
