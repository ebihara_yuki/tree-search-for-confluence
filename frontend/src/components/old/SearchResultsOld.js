// @flow

import React, { Component } from "react";
import type { PathElementId } from "./SearchResultNode";
import SearchResultNode from "./SearchResultNode";
import "../../css/TreeSearch.css";
import Button, { ButtonGroup } from "@atlaskit/button";
import type { DSearchResultTreeNode } from "../search/model/DSearchResultTreeNode";

// Will be replaced by url from backend response
const DEFAULT_BASE_URL = "";

export const EXPAND_STATE_EXPANDED: "EXPANDED" = "EXPANDED";
export const EXPAND_STATE_COLLAPSED: "COLLAPSED" = "COLLAPSED";

export type EXPAND_STATE =
    | typeof EXPAND_STATE_EXPANDED
    | typeof EXPAND_STATE_COLLAPSED;

type Props = {
    confluenceUrl: string,
    tree: DSearchResultTreeNode,
    pageAgeing: boolean
};

type State = {
    expandState: Object,
    // the "default" global state is either 'all are open' or 'all are closed'
    defaultExpandState: EXPAND_STATE
};

export default class SearchResultsOld extends Component<Props, State> {
    static defaultProps = {
      confluenceUrl: DEFAULT_BASE_URL
    };

    state = {
      // start expanded and allow user to collapse
      defaultExpandState: EXPAND_STATE_EXPANDED,
      expandState: {}
    };

    setExpanded = (id: PathElementId, expanded: EXPAND_STATE) => {
      this.state.expandState[id] = expanded;

      this.setState({
        expandState: Object.assign({}, this.state.expandState)
      });
    };

    setAllExpanded = (allExpandState: EXPAND_STATE) => {
      this.setState({
        defaultExpandState: allExpandState,
        expandState: {}
      });
    };

    isExpanded = (myId: PathElementId): boolean => {
      const expandState =
            this.state.expandState[myId] || this.state.defaultExpandState;
      return expandState === EXPAND_STATE_EXPANDED;
    };

    renderTree() {
      const { tree } = this.props;

      const traverse = (children: DSearchResultTreeNode[]): any => {
        return children.map(
          (child: DSearchResultTreeNode): any => {
            return {
              id: child.id,
              hasChildren: child.children.length > 0,
              content: child.pathElement.title,
              children: traverse(child.children)
            };
          }
        );
      };

      return traverse(tree.children);
    }

    render() {
      const { tree, confluenceUrl, pageAgeing } = this.props;

      const tableTree = this.renderTree();

      const oldResultsElement = buildNestedResultsElement(
        tree.children,
        confluenceUrl,
        pageAgeing,
        this.isExpanded,
        this.setExpanded
      );

      return (
        <div className="_ts_search_results">
          <p className="_ts_search_results__total-results">
                    Total results: {tree.count}
          </p>
          <ButtonGroup>
            <Button
              appearance="subtle-link"
              onClick={() => this.setAllExpanded(EXPAND_STATE_EXPANDED)}
            >
                        Expand All
            </Button>
            <Button
              appearance="subtle-link"
              onClick={() => this.setAllExpanded(EXPAND_STATE_COLLAPSED)}
            >
                        Collapse All
            </Button>
          </ButtonGroup>
          {oldResultsElement}
        </div>
      );
    }
}

export const buildNestedResultsElement = function(
  children: DSearchResultTreeNode[],
  confluenceUrl: string,
  pageAgeing: boolean,
  isExpanded: PathElementId => boolean,
  setExpanded: (id: PathElementId, expandState: EXPAND_STATE) => void
) {
  return (
    <ul className="_ts_results_list">
      {children.map(value => {
        return (
          <li key={value.id} className="_ts_results_list_item">
            <SearchResultNode
              data={value}
              baseUrl={confluenceUrl}
              pageAgeing={pageAgeing}
              isExpanded={isExpanded}
              setExpanded={setExpanded}
            />
          </li>
        );
      })}
    </ul>
  );
};
