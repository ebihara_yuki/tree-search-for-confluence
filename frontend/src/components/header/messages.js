// @flow

import { defineMessages } from "react-intl";

export default defineMessages({
  heading: {
    id: "confluencetreesearch.header.title",
    defaultMessage: "Confluence Tree Search",
    description: ""
  },
  body: {
    id: "confluencetreesearch.header.body",
    defaultMessage:
      "This application performs deep search in Confluence and displays results in convenient tree form.",
    description: ""
  },
  tooltip: {
    id: "confluencetreesearch.header.toolip",

    defaultMessage:
      "Search Confluence in deep tree format with an emphasis on context.",
    description: ""
  }
});
