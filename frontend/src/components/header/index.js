// @flow

import React, { PureComponent } from "react";
import { injectIntl, type IntlShape } from "react-intl";
import styled from "styled-components";
import InfoIcon from "@atlaskit/icon/glyph/info";
import AkTooltip from "@atlaskit/tooltip";
import messages from "./messages";
import { gridSize, textColor } from "../common/styles";

const TitleWrapper = styled.div`
  display: flex;
  margin-top: ${gridSize * 2}px;
  flex: 1;
  align-items: center;
`;
const Title = styled.div`
  font-size: 24px;
  font-weight: 500;
  color: ${textColor};
`;
const Icon = styled.div`
  margin-left: ${gridSize}px;
  padding-top: ${gridSize / 2}px;
`;

const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  margin-top: 32px;
`;

type Props = {
  intl: IntlShape
};

export class Header extends PureComponent<Props> {

  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <HeaderWrapper>
        <TitleWrapper>
          <Title>{formatMessage(messages.heading)}</Title>
          <Icon>
            <AkTooltip
              content={formatMessage(messages.tooltip)}
              position="right"
              delay={0}
            >
              <InfoIcon size="small" />
            </AkTooltip>
          </Icon>
        </TitleWrapper>
      </HeaderWrapper>
    );
  }
}

export default injectIntl(Header);
