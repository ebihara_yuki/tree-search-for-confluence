/**
 *
 *  This module queries Confluence Search and transforms result into convenient format.
 *
 *  Request to Confluence shall have the following shape and parameters:
 *
 *  /rest/api/search?
 *
 */

import type { DTransformedConfluenceResultEntity } from "./model/DTransformedConfluenceResultEntity";
import PathElement from "../components/PathElement";

const EXPAND =
  "content.ancestors,content.space.icon,content.history.contributors.publishers.users";
const EXCERPT = "highlight_unescaped&includeArchivedSpaces=false";


const currentTime = function () {
  const d = new Date();
  return d.getTime();
};

export function confluenceSearchNextPage(searchUrl, callback, errorCallback) {
  let requestStartTime = currentTime();
  window.AP.request({
    url: searchUrl,
    type: "GET",

    success: responseText => {
      const transformedResponse = transformResponse(responseText);
      const callDuration = currentTime() - requestStartTime;
      callback(transformedResponse, callDuration);
    },

    error: (xhr, statusText, errorThrown) => {
      const error = JSON.parse(errorThrown);
      if (error.statusCode === 400) {
        errorCallback("Please enter a valid CQL syntax.");
      } else {
        errorCallback("Unknown error occurred");
      }
      console.log("ERROR happened:" + statusText + ":" + errorThrown);
    }
  });
}

export default function confluenceSearch(
    cql: string,
    startPage: number,
    pageSize: number,
    callback,
    errorCallback: ErrorCallback
) {

  let searchUrl = `/rest/api/search?expand=${EXPAND}&excerpt=${EXCERPT}&cql=${encodeURI(
      cql
  )}&start=${startPage}&limit=${pageSize}`;

  confluenceSearchNextPage(searchUrl, callback, errorCallback);
}


function transformResponse(response: string) {
  const responseObject = JSON.parse(response);
  const transformedResults = [];
  responseObject.results.forEach(result => {
    try {
      const transformedResult = transform(result);
      if (transformedResult) {
        transformedResults.push(transformedResult);
      }
    } catch (ex) {
      console.log("Error happened while parsing search result. " + ex);
    }
  });

  return { results: transformedResults, next: responseObject._links.next };
}

function transform(resultObject: Object): DTransformedConfluenceResultEntity {
  const content = resultObject.content;
  if (!content) {
    console.log("Bad result. Search result does not have content. " +
        JSON.stringify(resultObject)
    );
    return null;
  }

  const singleSearchResult = getSingleSearchResult(resultObject, content);

  const path = buildPath(content);

  return {
    path: path,
    page: singleSearchResult
  };
}

function buildPath(content): PathElement[] {
  const path = [];

  const space = content.space;

  if (space) {
    path.push(buildPathElementFromSpace(space));
  }

  content.ancestors.forEach(ancestor =>
    path.push(buildPathElementFromContent(ancestor))
  );

  path.push(buildPathElementFromContent(content));
  return path;
}

function buildPathElementFromSpace(space) {
  const spaceKey = space.key;

  const icon = space.icon;

  return {
    id: space.id,
    title: space.name,
    link: "/display/" + spaceKey,
    icon: icon || null
  };
}

function buildPathElementFromContent(content): PathElement {
  return {
    id: content.id,
    content: content.title,
    link: content._links.webui,
    title: content.title
  };
}

function getSingleSearchResult(searchResult, content): PathElement {
  return {
    id: content.id,
    type: content.type,
    links: content._links,
    excerpt: searchResult.excerpt,
    title: searchResult.title,
    friendlyLastModified: searchResult.friendlyLastModified,
    timestamp: Date.parse(searchResult.lastModified),
    contributors: content.history.contributors.publishers.users.filter(
      user => user.accountId
    )
  };
}
