// @flow
import type { DPathElement } from "./model/DPathElement";
import type { DSearchResultTreeNode } from "./model/DSearchResultTreeNode";
import {
  addNodeToTree,
  createNewNodeWithPathElement,
  getNodeChild,
  newRootNode
} from "./model/DSearchResultTreeNode";
import type { DTransformedConfluenceResultEntity } from "./model/DTransformedConfluenceResultEntity";
import type { DTransformedConfluenceResultsResponse } from "./model/DTransformedConfluenceResultsResponse";
import type { DTreeSearchResult } from "./model/DTreeSearchResult";
import { fromSearchResultTreeNode } from "./model/DTreeSearchResult";
import type { DSingleSearchResult } from "./model/DSingleSearchResult";
import confluenceSearch, { confluenceSearchNextPage } from "./SearchClient";

const DEFAULT_PAGE_SIZE = 15;

/**
 * CQL Reference.
 *
 * Useful query patterns:
 * ancestor="168584937" - search only pages which have "168584937" as ancestor
 * contributor.accountId="accountId" - search by pages which this contributor by accountId
 *
 */
export type ProgressCallback = DTreeSearchResult => void;
export type CompleteCallback = void => void;
export type ErrorCallback = string => void;

export function startTreeLoadProgressivelyByCQL(
  cql: string,
  maxResults: number,
  progress: ProgressCallback,
  complete: CompleteCallback,
  errorCallback: ErrorCallback
) {
  const tree: DSearchResultTreeNode = newRootNode();

  searchNextPage(
    cql,
    0,
    DEFAULT_PAGE_SIZE,
    maxResults,
    tree,
    progress,
    complete,
    errorCallback
  );
}

function deepCopy(obj) {
  return JSON.parse(JSON.stringify(obj));
}

function searchNextPage(
  cql: string,
  startPage: number,
  pageSize: number,
  maxResults: number,
  tree: DSearchResultTreeNode,
  progress: ProgressCallback,
  complete: CompleteCallback,
  errorCallback: ErrorCallback,
  nextPageUrl: string
) {
  const processTreeResponseCallback = (transformedResult, duration) => {

    mergeAllResultsToTree(tree, transformedResult, startPage);

    calculateAndSetLatestModifiedTimestamp(tree);
    calculateAndSetNodeCounts(tree);
    calculateAndSetRank(tree);

    const result: DTreeSearchResult = fromSearchResultTreeNode(tree);
    //We need to create a deep copy of result to prevent data corruption by the "progress" method.
    //Also in case if result will be set to react state, we'll prevent state modification without calling setState()
    progress(deepCopy(result));

    const loadedResultsCount = transformedResult.results.length;

    const currentPage = loadedResultsCount + startPage;
    if (currentPage < maxResults && loadedResultsCount === pageSize) {
      searchNextPage(
        cql,
        currentPage,
        Math.min(pageSize, maxResults - currentPage),
        maxResults,
        tree,
        progress,
        complete,
        errorCallback,
        transformedResult.next
      );
    } else {
      complete();
    }
  };

  if(nextPageUrl) {
    confluenceSearchNextPage(nextPageUrl,
        processTreeResponseCallback,
        errorCallback);
  } else {
    confluenceSearch(
        cql,
        startPage,
        pageSize,
        processTreeResponseCallback,
        errorCallback
    );
  }
}

function mergeAllResultsToTree(
  root: DSearchResultTreeNode,
  transformedConfluenceResultsResponse: DTransformedConfluenceResultsResponse,
  currentPage: number
): void {
  transformedConfluenceResultsResponse.results.forEach((elem, index) => {
    elem.page.rank = index + currentPage;
    try {
      mergeResultToTree(root, elem);
    } catch (exception) {
      console.log("Error happened while processing single search result",
        exception
      );
    }
  });
}

function mergeResultToTree(
  root: DSearchResultTreeNode,
  entity: DTransformedConfluenceResultEntity
): DSearchResultTreeNode {
  const path = entity.path;
  merge(root, path, 0, entity.page);
  return root;
}

function merge(
  node: DSearchResultTreeNode,
  path: DPathElement[],
  pathPosition: number,
  value: DSingleSearchResult
) {
  if (path.length === pathPosition) {
    setNodeValue(node, value);
    return;
  }

  const currentPathElement: DPathElement = path[pathPosition];
  const child: ?DSearchResultTreeNode = getNodeChild(
    node,
    currentPathElement.id
  );

  if (child) {
    merge(child, path, pathPosition + 1, value);
  } else {
    const newNode: DSearchResultTreeNode = createNewNodeWithPathElement(
      currentPathElement
    );
    addNodeToTree(node, newNode);
    merge(newNode, path, pathPosition + 1, value);
  }
}

function setNodeValue(node: DSearchResultTreeNode, value: DSingleSearchResult) {
  const currentNodeValue: DSingleSearchResult | void = node.result;
  if (currentNodeValue !== null) {
    // console.log(
    //   "Tree collision. Node [1] value overridden. [2] Old, [3] New",
    //   node.pathElement,
    //   node.result,
    //   value
    // );
  }
  node.result = value;
}

const getChildrenMaxTimestamp = function(children) {
  let max = 0;
  children.forEach(child => {
    const childLastModified = calculateAndSetLatestModifiedTimestamp(child);
    if (childLastModified > max) {
      max = childLastModified;
    }
  });
  return max;
};

function calculateAndSetLatestModifiedTimestamp(
  treeNode: DSearchResultTreeNode
): number {
  const children = treeNode.children;

  let max = 0;

  if (treeNode.result) {
    max = treeNode.result.timestamp;
  }

  max = Math.max(max, getChildrenMaxTimestamp(children));

  treeNode.lastModificationTimestamp = max;
  return max;
}

function calculateAndSetNodeCounts(treeNode: DSearchResultTreeNode) {
  const children = treeNode.children;
  let count = treeNode.result ? 1 : 0;
  count = count + getChildrenCounts(children);
  treeNode.count = count;
  return count;
}

const getChildrenCounts = function(children) {
  let count = 0;
  children.forEach(child => {
    count = count + calculateAndSetNodeCounts(child);
  });
  return count;
};

const calculateAndSetRank = (treeNode: DSearchResultTreeNode): number => {
  const resultRank = treeNode.result ? treeNode.result.rank : Number.MAX_VALUE;
  if (treeNode.children.length) {
    treeNode.rank = Math.min(
      resultRank,
      ...treeNode.children.map(calculateAndSetRank)
    );

    return treeNode.rank;
  }

  treeNode.rank = resultRank;

  return resultRank;
};
