// @flow

import type {DTransformedConfluenceResultEntity} from "./DTransformedConfluenceResultEntity";

export type DTransformedConfluenceResultsResponse = {
  results: DTransformedConfluenceResultEntity[];
  confluenceUrl: string;
}