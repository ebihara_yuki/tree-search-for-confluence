// @flow
import type { ContentId } from "./DContentId";

export type DSingleSearchResult = {
  id: ContentId,
  type: string,
  title: string,
  excerpt: string,
  links: Object,
  friendlyLastModified: string,
  timestamp: number,
  rank: number
};
