// @flow
import type { DIcon } from "./DIcon";
import type { ContentId } from "./DContentId";

export type Contributor = {
  accountId: string,
  displayName: string,
  email: string,
  profilePicture: {
    path: string,
    width: number,
    height: number
  }
};

export type DPathElement = {
  id: ContentId,
  title: string,
  link: string,
  icon: DIcon | void,
  contributors: Contributor[]
};

export function newRootPathElement(): DPathElement {
  return {
    id: 0,
    title: "",
    link: "",
    icon: undefined,
    contributors: []
  };
}
