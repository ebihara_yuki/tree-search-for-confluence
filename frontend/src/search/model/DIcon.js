// @flow

export type DIcon = {
  path: string;
  width: number;
  height: number;
  isDefault: boolean;
}
