// @flow
import { newRootPathElement, type DPathElement } from "./DPathElement";
import type { DIcon } from "./DIcon";
import type { DSingleSearchResult } from "./DSingleSearchResult";
import type { ContentId } from "./DContentId";

export type DSearchResultTreeNode = {
  id?: number,
  pathElement: DPathElement,
  count: number,
  icon?: DIcon,
  result?: DSingleSearchResult,
  rank: number,
  children: DSearchResultTreeNode[],
  lastModificationTimestamp: number
};

export function getNodeChild(
  node: DSearchResultTreeNode,
  id: ContentId
): ?DSearchResultTreeNode {
  return node.children.find(child => child.id === id);
}

export function incNodeCount(node: DSearchResultTreeNode) {
  node.count = node.count ? node.count + 1 : 1;
}

export function createNewNodeWithPathElement(
  pathElement: DPathElement
): DSearchResultTreeNode {
  return {
    id: pathElement.id,
    pathElement: pathElement,
    children: [],
    count: 0,
    lastModificationTimestamp: 0,
    rank: 0
  };
}

export function addNodeToTree(
  tree: DSearchResultTreeNode,
  node: DSearchResultTreeNode
) {
  const id: ContentId = node.pathElement.id;

  const child = getNodeChild(tree, id);
  if (child) {
    throw new Error("Node already has child with id: " + id);
  }

  tree.children.push(node);

  return tree;
}

export function newRootNode(): DSearchResultTreeNode {
  return {
    id: 0,
    pathElement: newRootPathElement(),
    count: 0,
    children: [],
    lastModificationTimestamp: 0,
    rank: 0
  };
}
