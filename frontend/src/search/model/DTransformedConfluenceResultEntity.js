// @flow
import type {DPathElement} from "./DPathElement";
import type {DSingleSearchResult} from "./DSingleSearchResult";

export type DTransformedConfluenceResultEntity = {

  path: DPathElement[];

  page: DSingleSearchResult;

}
