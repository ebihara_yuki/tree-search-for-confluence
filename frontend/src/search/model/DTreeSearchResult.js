// @flow

import type { DSearchResultTreeNode } from "./DSearchResultTreeNode";

export type DTreeSearchResult = {
  totalResults: number,
  confluenceUrl: string,
  tree: DSearchResultTreeNode
};

export function fromSearchResultTreeNode(
  node: DSearchResultTreeNode
): DTreeSearchResult {
  return {
    tree: node,
    totalResults: node.count,
    confluenceUrl: ""
  };
}
